package view;

import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;

import controller.Controller;

public class PAT_View 
{
	private static Scanner scanner = new Scanner(System.in);

	public static void main (String[] args) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		boolean terminado = false;
		while(!terminado)
		{
			//try 
			//{
			System.out.println("\nEscoga la opci�n");
			System.out.println("1. Crear tablas municipio");
			System.out.println("2. Crear consolidado Departamento");
			System.out.println("3. Crear consolidado de un conjunto de municipios");
			System.out.println("4. Crear consolidado pa�s sin incluir capitales");
			System.out.println("5. Crear consolidado pa�s solo incluyendo capitales");
			System.out.println("6. Crear consolidado pa�s");
			System.out.println("7. Crear tablas de varios municipios");
			System.out.println("8. Crear tablas de varios departamentos");
			System.out.println("9. Bullshitear");
			System.out.println("10 Salir");
			int opcion = scanner.nextInt();
			switch (opcion) 
			{

			//Tablas municipio
			case 1:
				System.out.println("Ingrese el nombre del municipio. Represente los espacios con '_'");
				String municipio = scanner.next();

				Controller.crearTablasMunicipio(municipio.replaceAll("_", " "));


				break;

				//Consolidado depto
			case 2:
				System.out.println("Ingrese el nombre del departamento. Represente los espacios con '_'");
				String depto = scanner.next();
				Controller.creatTablasDepto(depto.replaceAll("_", " "));

				break;

				//Consolidado conjunto de municipios
			case 3:
				System.out.println("Ingrese el nombre de los municipios, separelos con '-'. Represente los espacios con '_'");
				String municipios = scanner.next();
				Controller.creatTablasConjutoMunicipios(municipios.replaceAll("_", " "));

				break;

			case 4:
				Controller.creatTablasPaisMenosCapitales();

				break;
				
			case 5:
				Controller.creatTablasPaisCapitales();

				break;
				
			case 6:
				Controller.creatTablasPais();

				break;
				
			case 7:
				System.out.println("Ingrese el nombre de los municipios, separelos con '-'. Represente los espacios con '_'");
				String municipiosRealizar = scanner.next();
				Controller.creatTablasVariosMunicipios(municipiosRealizar.replaceAll("_", " "));
				break;
				
			case 8:
				System.out.println("Ingrese el nombre de los departamentos, separelos con '-'. Represente los espacios con '_'");
				String departamentosRealizar = scanner.next();
				Controller.creatTablasVariosDepartamentos(departamentosRealizar.replaceAll("_", " "));
				break;
				
			case 9:
				System.out.println("Ingrese el nombre de los departamentos, separelos con '-'. Represente los espacios con '_'");
				String prueba = scanner.next();
				Controller.prueba(prueba.replaceAll("_", " "));
				break;

			case 10:
				terminado = true;
				break;
			}
			//}
			//catch (IOException | SAXException | ParserConfigurationException | OpenXML4JException e)
			//{
			//
			//}
		}
	}
}
