package VO;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 
 * @author Juli�nMontoya
 * Modela la informacion respectiva a una  pregunta indicativa
 */
public class VOData 
{
	private static final double VALOR_OSTENSIBLEMENTE_MAYOR = 0.1;
	private static final int VALOR_OSTENSIBLEMENTE_MENOR = 10;

	/**
	 * Columna L (12)
	 */
	private BigDecimal respuestaNecesidadPlaneacionMunicipio;

	/**
	 *  Columna N (14)
	 */
	private BigDecimal compromisoDefinitivoMunicipio;

	/**
	 * Columna 	P (16)
	 */
	private BigDecimal presupuestoDefinitivoMunicipio;

	/**
	 * Columna R (18)
	 */
	private BigDecimal compromisoDefinitivoDepartamento;

	/**
	 * Columna T (21)
	 */
	private BigDecimal presupuestoDefinitivoDepartamento;

	/**
	 * Columna U (21)
	 */
	private BigDecimal seguimientoCompromisoPrimerSemMunicipio;

	/**
	 * Columna W (23)
	 */
	private BigDecimal seguimientoCompromisoSegundoSemMunicipio;

	/**
	 * Columna V (22)
	 */
	private BigDecimal seguimientoPresupuestoPrimerSemMunicipio;

	/**
	 * Columna X (24)
	 */
	private BigDecimal seguimientoPresupuestoSegundoSemMunicipio;

	/**
	 * Columna Y (25)
	 */
	private BigDecimal seguimientoCompromisoPrimerSemDepartamento;

	/**
	 * Columna AA (27)
	 */
	private BigDecimal seguimientoCompromisoSegundoSemDepartamento;

	/**
	 * Columna (26)
	 */
	private BigDecimal seguimientoPresupuestoPrimerSemDepartamento;

	/**
	 * Columna AB (28)
	 */
	private BigDecimal seguimientoPresupuestoSegundoSemDepartamento;

	/**
	 * Modela un objeto thath keeps traks of all values that were null in the PAT file
	 */
	private VODataNull dataNull;

	/**
	 * Modela un objeto con los resultados de las inconsistencias
	 */
	private VOInconsistencia inconsistencias;

	//M�TODOS

	/**
	 * Da el porcetaje de necesidad cubiertas sobre el compromiso
	 * @return porcentaje
	 */
	public BigDecimal getPorcentajeNecesidadesCubiertasConCompromiso()
	{
		try
		{
			return (compromisoDefinitivoDepartamento.add(compromisoDefinitivoMunicipio)).divide(respuestaNecesidadPlaneacionMunicipio, 2 , RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Da el porcetaje de necesidad cubiertas en la ejecuci�n
	 * @return porcentaje
	 */
	public BigDecimal getPorcentajeNecesidadesCubiertasConEjecucion()
	{
		try
		{
			return (darEjecucionDepartamento().add(darEjecucionMunicipio())).divide(respuestaNecesidadPlaneacionMunicipio,2 , RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Da el porcetaje de necesidad cubiertas en la ejecuci�n del departamento
	 * @return porcentaje
	 */
	public BigDecimal getPorcentajeNecesidadesCubiertasConEjecucionDepartamento()
	{
		try
		{
			return darEjecucionDepartamento().divide(respuestaNecesidadPlaneacionMunicipio,2 , RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Da el porcetaje de necesidad cubiertas en la ejecuci�n del municipio
	 * @return porcentaje
	 */
	public BigDecimal getPorcentajeNecesidadesCubiertasConEjecucionMunicipio()
	{
		try
		{
			return darEjecucionMunicipio().divide(respuestaNecesidadPlaneacionMunicipio,2 , RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Retorna la ejecucion del municipio
	 * @return ejecucion del municipio
	 */
	public BigDecimal darEjecucionMunicipio()
	{
		return (seguimientoCompromisoPrimerSemMunicipio.add(seguimientoCompromisoSegundoSemMunicipio));
	}

	/**
	 * Retorna el seguimiento de ejecucion del presupuesto del municipio
	 * @return ejecucion seguimiento presupuesto municipio
	 */
	public BigDecimal darPresupuestoEjecucionMunicipio()
	{
		return seguimientoPresupuestoPrimerSemMunicipio.add(seguimientoPresupuestoSegundoSemMunicipio);
	}

	/**
	 * Retorna la ejecucion del departamento
	 * @return ejecucion del departamento
	 */
	public BigDecimal darEjecucionDepartamento()
	{
		return seguimientoCompromisoPrimerSemDepartamento.add(seguimientoCompromisoSegundoSemDepartamento);
	}

	/**
	 * Retorna el seguimiento de ejecucion del presupuesto del departamento
	 * @return ejecucion seguimiento presupuesto departamento
	 */
	public BigDecimal darPresupuestoEjecucionDepartamento()
	{
		return seguimientoPresupuestoPrimerSemDepartamento.add(seguimientoPresupuestoSegundoSemDepartamento);
	}

	/**
	 * Da el porcentaje de ejecucion segun los compromisos del municipio
	 * @return porcentaje ejecucion sobre los compromisos
	 */
	public BigDecimal darPorcentajeEjecucionSobreCompromisosMunicipio()
	{
		try
		{
			return darPresupuestoEjecucionMunicipio().divide(presupuestoDefinitivoMunicipio, 2, RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Da el porcentaje de ejecucion segun los compromisos del departamento
	 * @return porcentaje ejecucion sobre los compromisos
	 */
	public BigDecimal darPorcentajeEjecucionSobreCompromisosDepartamento()
	{
		try
		{
			return darPresupuestoEjecucionDepartamento().divide(presupuestoDefinitivoDepartamento,2 , RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Da el porcentaje de ejecucion segun los compromisos totales
	 * @return porcentaje ejecucion sobre los compromisos
	 */
	public BigDecimal darPorcentajeEjecucionSobreCompromisosTotal()
	{
		try
		{
			return (darPresupuestoEjecucionDepartamento().add(darPresupuestoEjecucionMunicipio())).divide(presupuestoDefinitivoDepartamento.add(presupuestoDefinitivoMunicipio),2 , RoundingMode.HALF_UP);
		}
		catch (ArithmeticException e) 
		{
			return new BigDecimal(-1);
		}
	}

	/**
	 * Suma la data actual con otra que llega por parametro
	 * @param data dada por par�metro
	 */
	public VOData sumarVOData(VOData data)
	{
		VOData retornar = new VOData();

		retornar.setRespuestaNecesidadPlaneacionMunicipio(this.respuestaNecesidadPlaneacionMunicipio.add(data.getRespuestaNecesidadPlaneacionMunicipio()));
		retornar.setCompromisoDefinitivoMunicipio(this.compromisoDefinitivoMunicipio.add(data.getCompromisoDefinitivoMunicipio())) ;
		retornar.setPresupuestoDefinitivoMunicipio(this.presupuestoDefinitivoMunicipio.add(data.getPresupuestoDefinitivoMunicipio())); 
		retornar.setCompromisoDefinitivoDepartamento(this.compromisoDefinitivoDepartamento.add(data.getCompromisoDefinitivoDepartamento()));
		retornar.setPresupuestoDefinitivoDepartamento(presupuestoDefinitivoDepartamento.add(data.getPresupuestoDefinitivoDepartamento()));
		retornar.setSeguimientoCompromisoPrimerSemMunicipio(this.seguimientoCompromisoPrimerSemMunicipio.add(data.getSeguimientoCompromisoPrimerSemMunicipio()));
		retornar.setSeguimientoCompromisoSegundoSemMunicipio(this.seguimientoCompromisoSegundoSemMunicipio.add(data.getSeguimientoCompromisoSegundoSemMunicipio()));
		retornar.setSeguimientoPresupuestoPrimerSemMunicipio(this.seguimientoPresupuestoPrimerSemMunicipio.add(data.getSeguimientoPresupuestoPrimerSemMunicipio()));
		retornar.setSeguimientoPresupuestoSegundoSemMunicipio(this.seguimientoPresupuestoSegundoSemMunicipio.add(data.getSeguimientoPresupuestoSegundoSemMunicipio()));

		retornar.setSeguimientoCompromisoPrimerSemDepartamento(this.seguimientoCompromisoPrimerSemDepartamento.add(data.getSeguimientoCompromisoPrimerSemDepartamento()));
		retornar.setSeguimientoCompromisoSegundoSemDepartamento(this.seguimientoCompromisoSegundoSemDepartamento.add(data.getSeguimientoCompromisoSegundoSemDepartamento()));
		retornar.setSeguimientoPresupuestoPrimerSemDepartamento(this.seguimientoPresupuestoPrimerSemDepartamento.add(data.getSeguimientoPresupuestoPrimerSemDepartamento()));
		retornar.setSeguimientoPresupuestoSegundoSemDepartamento(this.seguimientoPresupuestoSegundoSemDepartamento.add(data.getSeguimientoPresupuestoSegundoSemDepartamento()));


		return retornar;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoCuandoNoSeReportaNecesidadMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullRespuestaNecesidadPlaneacionMunicipio() && !dataNull.isNullcompromisoDefinitivoMunicipio() &&(respuestaNecesidadPlaneacionMunicipio.compareTo(cero) == 0) && (compromisoDefinitivoMunicipio.compareTo(cero) != 0))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoSuperiorANecesidadMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullRespuestaNecesidadPlaneacionMunicipio() && ! dataNull.isNullcompromisoDefinitivoMunicipio() && (respuestaNecesidadPlaneacionMunicipio.compareTo(cero) != 0) && (compromisoDefinitivoMunicipio.compareTo(respuestaNecesidadPlaneacionMunicipio) == 1))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoSinPresupuestoMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullpresupuestoDefinitivoMunicipio() && !dataNull.isNullcompromisoDefinitivoMunicipio() && (compromisoDefinitivoMunicipio.compareTo(cero) != 0) && (presupuestoDefinitivoMunicipio.compareTo(cero) == 0))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int presupuestoSinCompromisoMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullpresupuestoDefinitivoMunicipio() && !dataNull.isNullcompromisoDefinitivoMunicipio() &&(presupuestoDefinitivoMunicipio.compareTo(cero) != 0) && (compromisoDefinitivoMunicipio.compareTo(cero) == 0))
		{
			return 1;
		}
		return 0;
	}	

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int necesidadSinDigilenciar()
	{
		if(dataNull.isNullRespuestaNecesidadPlaneacionMunicipio())
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoSinDigilenciarMunicipio()
	{
		if(dataNull.isNullcompromisoDefinitivoMunicipio())
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoSinDigilenciarDepto()
	{
		BigDecimal cero = new BigDecimal(0);
		if(dataNull.isNullcompromisoDefinitivoDepartamento() && !dataNull.isNullRespuestaNecesidadPlaneacionMunicipio() && respuestaNecesidadPlaneacionMunicipio.compareTo(cero) != 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int presupuestoSinDigilenciarMunicipio()
	{
		if(dataNull.isNullpresupuestoDefinitivoMunicipio())
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int presupuestoSinDigilenciarDepto()
	{
		BigDecimal cero = new BigDecimal(0);
		if(dataNull.isNullPresupuestoDefinitivoDepartamento()  && !dataNull.isNullRespuestaNecesidadPlaneacionMunicipio() && respuestaNecesidadPlaneacionMunicipio.compareTo(cero) != 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoCuandoNoSeReportaNecesidadDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullRespuestaNecesidadPlaneacionMunicipio() && !dataNull.isNullcompromisoDefinitivoDepartamento() &&(respuestaNecesidadPlaneacionMunicipio.compareTo(cero) == 0) && (compromisoDefinitivoDepartamento.compareTo(cero) != 0))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoSuperiorANecesidadDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullRespuestaNecesidadPlaneacionMunicipio() && ! dataNull.isNullcompromisoDefinitivoDepartamento() && (respuestaNecesidadPlaneacionMunicipio.compareTo(cero) != 0) && (compromisoDefinitivoDepartamento.compareTo(respuestaNecesidadPlaneacionMunicipio) == 1))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int compromisoSinPresupuestoDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullPresupuestoDefinitivoDepartamento() && !dataNull.isNullcompromisoDefinitivoDepartamento() && (compromisoDefinitivoDepartamento.compareTo(cero) != 0) && (presupuestoDefinitivoDepartamento.compareTo(cero) == 0))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int presupuestoSinCompromisoDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!dataNull.isNullPresupuestoDefinitivoDepartamento() && !dataNull.isNullcompromisoDefinitivoDepartamento() && (presupuestoDefinitivoDepartamento.compareTo(cero) != 0) && (compromisoDefinitivoDepartamento.compareTo(cero) == 0))
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ejecucionFisicaSinPresupuestoMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!ambosNullSeguimientoSeguimientoMunicipio() && !ambosNullSeguimientoPresupuestoMunicipio() && (darEjecucionMunicipio().compareTo(cero)) != 0 && darPresupuestoEjecucionMunicipio().compareTo(cero) == 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int presupuestoSinEjecucionFisicaMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!ambosNullSeguimientoSeguimientoMunicipio() && !ambosNullSeguimientoPresupuestoMunicipio() && (darEjecucionMunicipio().compareTo(cero)) == 0 && darPresupuestoEjecucionMunicipio().compareTo(cero) != 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si se presenta una vez la inconsistencia, 2 si se presenta en ambos valores y 0 si no se presenta
	 * @return int indicativo de inconsistencias
	 */
	public int ejecucionFisicaSinDigilenciarMunicipio()
	{
		int ret = 0;
		if(dataNull.isNullSeguimientoCompromisoPrimerSemMunicipio())
		{
			ret++;
		}
		if(dataNull.isNullSeguimientoCompromisoSegundoSemMunicipio())
		{
			ret++;
		}
		return ret;
	}

	/**
	 * Retorna 1 si se presenta una vez la inconsistencia, 2 si se presenta en ambos valores y 0 si no se presenta
	 * @return int indicativo de inconsistencias
	 */
	public int presupuestoEjecucionSinDigilenciarMunicipio()
	{
		int ret = 0;
		if(dataNull.isNullSeguimientoPresupuestoPrimerSemMunicipio())
		{
			ret++;
		}
		if(dataNull.isNullSeguimientoPresupuestoSegundoSemMunicipio())
		{
			ret++;
		}
		return ret;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ejecucionFisicaSinPresupuestoDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!ambosNullSeguimientoSeguimientoDepartamento() && !ambosNullSeguimientoPresupuestoDepartamento() && (darEjecucionDepartamento().compareTo(cero)) != 0 && darPresupuestoEjecucionDepartamento().compareTo(cero) == 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int presupuestoSinEjecucionFisicaDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(!ambosNullSeguimientoSeguimientoDepartamento() && !ambosNullSeguimientoPresupuestoDepartamento() && (darEjecucionDepartamento().compareTo(cero)) == 0 && darPresupuestoEjecucionDepartamento().compareTo(cero) != 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si se presenta una vez la inconsistencia, 2 si se presenta en ambos valores y 0 si no se presenta
	 * @return int indicativo de inconsistencias
	 */
	public int ejecucionFisicaSinDigilenciarDepartamento()
	{
		int ret = 0;
		if(dataNull.isNullSeguimientoCompromisoPrimerSemDepartamento())
		{
			ret++;
		}
		if(dataNull.isNullSeguimientoCompromisoSegundoSemDepartamento())
		{
			ret++;
		}
		return ret;
	}

	/**
	 * Retorna 1 si se presenta una vez la inconsistencia, 2 si se presenta en ambos valores y 0 si no se presenta
	 * @return int indicativo de inconsistencias
	 */
	public int presupuestoEjecucionSinDigilenciarDepartamento()
	{
		int ret = 0;
		if(dataNull.isNullSeguimientoPresupuestoPrimerSemDepartamento())
		{
			ret++;
		}
		if(dataNull.isNullSeguimientoPresupuestoSegundoSemDepartamento())
		{
			ret++;
		}
		return ret;
	}

	/**
	 * Retorna true si ambos valores de seguimiento del presupuesto del municipio son null
	 * @return true si ambos son null, falso de lo contratrio
	 */
	public boolean ambosNullSeguimientoPresupuestoMunicipio()
	{
		return dataNull.isNullSeguimientoPresupuestoPrimerSemMunicipio() && dataNull.isNullSeguimientoPresupuestoSegundoSemMunicipio();
	}

	/**
	 * Retorna true si ambos valores de seguimiento del seguimiento del municipio son null
	 * @return true si ambos son null, falso de lo contratrio
	 */
	public boolean ambosNullSeguimientoSeguimientoMunicipio()
	{
		return dataNull.isNullSeguimientoCompromisoPrimerSemMunicipio() && dataNull.isNullSeguimientoCompromisoSegundoSemMunicipio();
	}

	/**
	 * Retorna true si ambos valores de seguimiento del presupuesto del Departamento son null
	 * @return true si ambos son null, falso de lo contratrio
	 */
	public boolean ambosNullSeguimientoPresupuestoDepartamento()
	{
		return dataNull.isNullSeguimientoPresupuestoPrimerSemDepartamento() && dataNull.isNullSeguimientoPresupuestoSegundoSemDepartamento();
	}

	/**
	 * Retorna true si ambos valores de seguimiento del seguimiento del Departamento son null
	 * @return true si ambos son null, falso de lo contratrio
	 */
	public boolean ambosNullSeguimientoSeguimientoDepartamento()
	{
		return dataNull.isNullSeguimientoCompromisoPrimerSemDepartamento() && dataNull.isNullSeguimientoCompromisoSegundoSemDepartamento();
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ostensiblementeMayorEnLaEjecucionMunicipio()
	{
		try
		{
			BigDecimal primerNum = presupuestoDefinitivoMunicipio.divide(compromisoDefinitivoMunicipio);
			BigDecimal segundoNum = (new BigDecimal(VALOR_OSTENSIBLEMENTE_MAYOR)).multiply(darPresupuestoEjecucionMunicipio().divide(darEjecucionMunicipio()));
			if(primerNum.compareTo(segundoNum) <= 0)
			{
				return 1;
			}
		}
		catch (ArithmeticException e) 
		{
			return 0;
		}

		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ostensiblementeInferiorEnLaEjecucionMunicipio()
	{
		try
		{
			BigDecimal primerNum = presupuestoDefinitivoMunicipio.divide(compromisoDefinitivoMunicipio);
			BigDecimal segundoNum = (new BigDecimal(VALOR_OSTENSIBLEMENTE_MENOR)).multiply(darPresupuestoEjecucionMunicipio().divide(darEjecucionMunicipio()));
			if(primerNum.compareTo(segundoNum) >= 0)
			{
				return 1;
			}
		}
		catch (ArithmeticException e) 
		{
			return 0;
		}

		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ejecutaPeroNoPlanificaMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(compromisoDefinitivoMunicipio.compareTo(cero) == 0 && presupuestoDefinitivoMunicipio.compareTo(cero) == 0 && darEjecucionMunicipio().compareTo(cero) != 0 && darPresupuestoEjecucionMunicipio().compareTo(cero) != 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int planificaPeroNoEjecutaMunicipio()
	{
		BigDecimal cero = new BigDecimal(0);
		if(compromisoDefinitivoMunicipio.compareTo(cero) != 0 && presupuestoDefinitivoMunicipio.compareTo(cero) != 0 && darEjecucionMunicipio().compareTo(cero) == 0 && darPresupuestoEjecucionMunicipio().compareTo(cero) == 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ostensiblementeMayorEnLaEjecucionDepartamento()
	{
		try
		{
			BigDecimal primerNum = presupuestoDefinitivoDepartamento.divide(compromisoDefinitivoDepartamento);
			BigDecimal segundoNum = (new BigDecimal(VALOR_OSTENSIBLEMENTE_MAYOR)).multiply(darPresupuestoEjecucionDepartamento().divide(darEjecucionDepartamento()));
			if(primerNum.compareTo(segundoNum) <= 0)
			{
				return 1;
			}
		}
		catch (ArithmeticException e) 
		{
			return 0;
		}

		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ostensiblementeInferiorEnLaEjecucionDepartamento()
	{
		try
		{
			BigDecimal primerNum = presupuestoDefinitivoDepartamento.divide(compromisoDefinitivoDepartamento);
			BigDecimal segundoNum = (new BigDecimal(VALOR_OSTENSIBLEMENTE_MENOR)).multiply(darPresupuestoEjecucionDepartamento().divide(darEjecucionDepartamento()));
			if(primerNum.compareTo(segundoNum) >= 0)
			{
				return 1;
			}
		}
		catch (ArithmeticException e) 
		{
			return 0;
		}

		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int ejecutaPeroNoPlanificaDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(compromisoDefinitivoDepartamento.compareTo(cero) == 0 && presupuestoDefinitivoDepartamento.compareTo(cero) == 0 && darEjecucionDepartamento().compareTo(cero) != 0 && darPresupuestoEjecucionDepartamento().compareTo(cero) != 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Retorna 1 si la inconsistencia se presenta, false de lo contrario
	 * @return boolean n�merico
	 */
	public int planificaPeroNoEjecutaDepartamento()
	{
		BigDecimal cero = new BigDecimal(0);
		if(compromisoDefinitivoDepartamento.compareTo(cero) != 0 && presupuestoDefinitivoDepartamento.compareTo(cero) != 0 && darEjecucionDepartamento().compareTo(cero) == 0 && darPresupuestoEjecucionDepartamento().compareTo(cero) == 0)
		{
			return 1;
		}
		return 0;
	}

	/**
	 * Calcula y retorna objeto de inconsitencias
	 * @return
	 */
	public VOInconsistencia calcularInconsistencias()
	{
		Integer[] incon = new Integer[35];
		int[] inconMunicip1 = new int[11];
		int[] inconDept1 = new int[10];
		
		int[] inconMunicip2 = new int[4];
		int[] inconDept2 = new int[4];

		int val0 = compromisoCuandoNoSeReportaNecesidadMunicipio();
		int val1 = compromisoSuperiorANecesidadMunicipio();
		int val2 = compromisoSinPresupuestoMunicipio();
		int val3 = presupuestoSinCompromisoMunicipio();
		int val4 = necesidadSinDigilenciar();
		int val5 = compromisoSinDigilenciarMunicipio();
		int val6 = presupuestoSinDigilenciarMunicipio();

		int val7 = compromisoCuandoNoSeReportaNecesidadDepartamento();
		int val8 = compromisoSuperiorANecesidadDepartamento();
		int val9 = compromisoSinPresupuestoDepartamento();
		int val10 = presupuestoSinCompromisoDepartamento();
		int val12 = compromisoSinDigilenciarDepto();
		int val13 = presupuestoSinDigilenciarDepto();

		int val14 = ejecucionFisicaSinPresupuestoMunicipio();
		int val15 = presupuestoSinEjecucionFisicaMunicipio();
		int val16 = ejecucionFisicaSinDigilenciarMunicipio();
		int val17 = presupuestoEjecucionSinDigilenciarMunicipio();

		int val18 = ejecucionFisicaSinPresupuestoDepartamento();
		int val19 = presupuestoSinEjecucionFisicaDepartamento();
		int val20 = ejecucionFisicaSinDigilenciarDepartamento();
		int val21 = presupuestoEjecucionSinDigilenciarDepartamento();

		int val22 = ostensiblementeMayorEnLaEjecucionMunicipio();
		int val23 = ostensiblementeInferiorEnLaEjecucionMunicipio();
		int val24 = ejecutaPeroNoPlanificaMunicipio();
		int val25 = planificaPeroNoEjecutaMunicipio();

		int val26 = ostensiblementeMayorEnLaEjecucionDepartamento();
		int val27 = ostensiblementeInferiorEnLaEjecucionDepartamento();
		int val28 = ejecutaPeroNoPlanificaDepartamento();
		int val29 = planificaPeroNoEjecutaDepartamento();

		//municip1
		incon[0] = val0;
		incon[1] = val1;
		incon[2] = val2;
		incon[3] = val3;
		incon[4] = val4;
		incon[5] = val5;
		incon[6] = val6;

		//dep1
		incon[7] = val7;
		incon[8] = val8;
		incon[9] = val9;
		incon[10] = val10;
		incon[11] = val12;
		incon[12] = val13;

		//municip2
		incon[13] = val14;
		incon[14] = val15;
		incon[15] = val16;
		incon[16] = val17;

		//dep2
		incon[17] = val18;
		incon[18] = val19;
		incon[19] = val20;
		incon[20] = val21;

		//municip3
		incon[24] = val22;
		incon[25] = val23;
		incon[26] = val24;
		incon[27] = val25;

		//dep3
		incon[28] = val26;
		incon[29] = val27;
		incon[30] = val28;
		incon[31] = val29;

		//municip1
		inconMunicip1[0] = val0;
		inconMunicip1[1] = val1;
		inconMunicip1[2] = val2;
		inconMunicip1[3] = val3;
		inconMunicip1[4] = val4;
		inconMunicip1[5] = val5;
		inconMunicip1[6] = val6;
		//municip2
		inconMunicip1[7] = val14;
		inconMunicip1[8] = val15;
		inconMunicip1[9] = val16;
		inconMunicip1[10] = val17;
		//municip3
		inconMunicip2[0] = val22;
		inconMunicip2[1] = val23;
		inconMunicip2[2] = val24;
		inconMunicip2[3] = val25;

		//dep1
		inconDept1[0] = val7;
		inconDept1[1] = val8;
		inconDept1[2] = val9;
		inconDept1[3] = val10;
		inconDept1[4] = val12;
		inconDept1[5] = val13;
		//dep2
		inconDept1[6] = val18;
		inconDept1[7] = val19;
		inconDept1[8] = val20;
		inconDept1[9] = val21;
		//dep3
		inconDept2[0] = val26;
		inconDept2[1] = val27;
		inconDept2[2] = val28;
		inconDept2[3] = val29;

		VOInconsistencia inconsistencias = new VOInconsistencia();
		inconsistencias.setResultInconsistencia(incon);
		inconsistencias.hayInconsistencia();
		inconsistencias.hayInconsistenciaMunicipioParte1(inconMunicip1);
		inconsistencias.hayInconsistenciaDelDepartamentoParte1(inconDept1);
		inconsistencias.hayInconsistenciaMunicipioParte2(inconMunicip2);
		inconsistencias.hayInconsistenciaDelDepartamentoParte2(inconDept2);
		return inconsistencias;
	}
	//GETTERS AND SETTERS

	/**
	 * @return the respuestaNecesidadPlaneacionMunicipio
	 */
	public BigDecimal getRespuestaNecesidadPlaneacionMunicipio() 
	{
		return respuestaNecesidadPlaneacionMunicipio;
	}

	/**
	 * @param respuestaNecesidadPlaneacionMunicipio the respuestaNecesidadPlaneacionMunicipio to set
	 */
	public void setRespuestaNecesidadPlaneacionMunicipio(BigDecimal respuestaNecesidadPlaneacionMunicipio)
	{
		this.respuestaNecesidadPlaneacionMunicipio = respuestaNecesidadPlaneacionMunicipio;
	}

	/**
	 * @return the compromisoDefinitivoMunicipio
	 */
	public BigDecimal getCompromisoDefinitivoMunicipio()
	{
		return compromisoDefinitivoMunicipio;
	}

	/**
	 * @param compromisoDefinitivoMunicipio the compromisoDefinitivoMunicipio to set
	 */
	public void setCompromisoDefinitivoMunicipio(BigDecimal compromisoDefinitivoMunicipio)
	{
		this.compromisoDefinitivoMunicipio = compromisoDefinitivoMunicipio;
	}

	/**
	 * @return the presupuestoDefinitivoMunicipio
	 */
	public BigDecimal getPresupuestoDefinitivoMunicipio()
	{
		return presupuestoDefinitivoMunicipio;
	}

	/**
	 * @param presupuestoDefinitivoMunicipio the presupuestoDefinitivoMunicipio to set
	 */
	public void setPresupuestoDefinitivoMunicipio(BigDecimal presupuestoDefinitivoMunicipio)
	{
		this.presupuestoDefinitivoMunicipio = presupuestoDefinitivoMunicipio;
	}

	/**
	 * @return the compromisoDefinitivoDepartamento
	 */
	public BigDecimal getCompromisoDefinitivoDepartamento() 
	{
		return compromisoDefinitivoDepartamento;
	}

	/**
	 * @param compromisoDefinitivoDepartamento the compromisoDefinitivoDepartamento to set
	 */
	public void setCompromisoDefinitivoDepartamento(BigDecimal compromisoDefinitivoDepartamento)
	{
		this.compromisoDefinitivoDepartamento = compromisoDefinitivoDepartamento;
	}

	/**
	 * @return the presupuestoDefinitivoDepartamento
	 */
	public BigDecimal getPresupuestoDefinitivoDepartamento() 
	{
		return presupuestoDefinitivoDepartamento;
	}

	/**
	 * @param presupuestoDefinitivoDepartamento the presupuestoDefinitivoDepartamento to set
	 */
	public void setPresupuestoDefinitivoDepartamento(BigDecimal presupuestoDefinitivoDepartamento)
	{
		this.presupuestoDefinitivoDepartamento = presupuestoDefinitivoDepartamento;
	}

	/**
	 * @return the seguimientoCompromisoPrimerSemMunicipio
	 */
	public BigDecimal getSeguimientoCompromisoPrimerSemMunicipio()
	{
		return seguimientoCompromisoPrimerSemMunicipio;
	}

	/**
	 * @param seguimientoCompromisoPrimerSemMunicipio the seguimientoCompromisoPrimerSemMunicipio to set
	 */
	public void setSeguimientoCompromisoPrimerSemMunicipio(BigDecimal seguimientoCompromisoPrimerSemMunicipio)
	{
		this.seguimientoCompromisoPrimerSemMunicipio = seguimientoCompromisoPrimerSemMunicipio;
	}

	/**
	 * @return the seguimientoCompromisoSegundoSemMunicipio
	 */
	public BigDecimal getSeguimientoCompromisoSegundoSemMunicipio()
	{
		return seguimientoCompromisoSegundoSemMunicipio;
	}

	/**
	 * @param seguimientoCompromisoSegundoSemMunicipio the seguimientoCompromisoSegundoSemMunicipio to set
	 */
	public void setSeguimientoCompromisoSegundoSemMunicipio(BigDecimal seguimientoCompromisoSegundoSemMunicipio) 
	{
		this.seguimientoCompromisoSegundoSemMunicipio = seguimientoCompromisoSegundoSemMunicipio;
	}

	/**
	 * @return the seguimientoPresupuestoPrimerSemMunicipio
	 */
	public BigDecimal getSeguimientoPresupuestoPrimerSemMunicipio()
	{
		return seguimientoPresupuestoPrimerSemMunicipio;
	}

	/**
	 * @param seguimientoPresupuestoPrimerSemMunicipio the seguimientoPresupuestoPrimerSemMunicipio to set
	 */
	public void setSeguimientoPresupuestoPrimerSemMunicipio(BigDecimal seguimientoPresupuestoPrimerSemMunicipio) 
	{
		this.seguimientoPresupuestoPrimerSemMunicipio = seguimientoPresupuestoPrimerSemMunicipio;
	}

	/**
	 * @return the seguimientoPresupuestoSegundoSemMunicipio
	 */
	public BigDecimal getSeguimientoPresupuestoSegundoSemMunicipio() 
	{
		return seguimientoPresupuestoSegundoSemMunicipio;
	}

	/**
	 * @param seguimientoPresupuestoSegundoSemMunicipio the seguimientoPresupuestoSegundoSemMunicipio to set
	 */
	public void setSeguimientoPresupuestoSegundoSemMunicipio(BigDecimal seguimientoPresupuestoSegundoSemMunicipio)
	{
		this.seguimientoPresupuestoSegundoSemMunicipio = seguimientoPresupuestoSegundoSemMunicipio;
	}

	/**
	 * @return the seguimientoCompromisoPrimerSemDepartamento
	 */
	public BigDecimal getSeguimientoCompromisoPrimerSemDepartamento() 
	{
		return seguimientoCompromisoPrimerSemDepartamento;
	}

	/**
	 * @param seguimientoCompromisoPrimerSemDepartamento the seguimientoCompromisoPrimerSemDepartamento to set
	 */
	public void setSeguimientoCompromisoPrimerSemDepartamento(BigDecimal seguimientoCompromisoPrimerSemDepartamento)
	{
		this.seguimientoCompromisoPrimerSemDepartamento = seguimientoCompromisoPrimerSemDepartamento;
	}

	/**
	 * @return the seguimientoCompromisoSegundoSemDepartamento
	 */
	public BigDecimal getSeguimientoCompromisoSegundoSemDepartamento()
	{
		return seguimientoCompromisoSegundoSemDepartamento;
	}

	/**
	 * @param seguimientoCompromisoSegundoSemDepartamento the seguimientoCompromisoSegundoSemDepartamento to set
	 */
	public void setSeguimientoCompromisoSegundoSemDepartamento(BigDecimal seguimientoCompromisoSegundoSemDepartamento)
	{
		this.seguimientoCompromisoSegundoSemDepartamento = seguimientoCompromisoSegundoSemDepartamento;
	}

	/**
	 * @return the seguimientoPresupuestoPrimerSemDepartamento
	 */
	public BigDecimal getSeguimientoPresupuestoPrimerSemDepartamento()
	{
		return seguimientoPresupuestoPrimerSemDepartamento;
	}

	/**
	 * @param seguimientoPresupuestoPrimerSemDepartamento the seguimientoPresupuestoPrimerSemDepartamento to set
	 */
	public void setSeguimientoPresupuestoPrimerSemDepartamento(BigDecimal seguimientoPresupuestoPrimerSemDepartamento) {
		this.seguimientoPresupuestoPrimerSemDepartamento = seguimientoPresupuestoPrimerSemDepartamento;
	}

	/**
	 * @return the seguimientoPresupuestoSegundoSemDepartamento
	 */
	public BigDecimal getSeguimientoPresupuestoSegundoSemDepartamento()
	{
		return seguimientoPresupuestoSegundoSemDepartamento;
	}

	/**
	 * @param seguimientoPresupuestoSegundoSemDepartamento the seguimientoPresupuestoSegundoSemDepartamento to set
	 */
	public void setSeguimientoPresupuestoSegundoSemDepartamento(BigDecimal seguimientoPresupuestoSegundoSemDepartamento)
	{
		this.seguimientoPresupuestoSegundoSemDepartamento = seguimientoPresupuestoSegundoSemDepartamento;
	}

	/**
	 * @return the dataNull
	 */
	public VODataNull getDataNull()
	{
		return dataNull;
	}

	/**
	 * @param dataNull the dataNull to set
	 */
	public void setDataNull(VODataNull dataNull)
	{
		this.dataNull = dataNull;
	}

	/**
	 * @return the inconsistencias
	 */
	public VOInconsistencia getInconsistencias() 
	{
		return inconsistencias;
	}

	/**
	 * @param inconsistencias the inconsistencias to set
	 */
	public void setInconsistencias(VOInconsistencia inconsistencias) 
	{
		this.inconsistencias = inconsistencias;
	}
}
