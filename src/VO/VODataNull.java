package VO;

/**
 * 
 * @author Juli�n Montoya
 * Esta clase es un VOData pero todos los atributos modelan un boolean que indica si el valor fue le�do como null o no
 */
public class VODataNull
{
	/**
	 * Columna L (12)
	 */
	private boolean esNullRespuestaNecesidadPlaneacionMunicipio;

	/**
	 *  Columna N (14)
	 */
	private boolean esNullcompromisoDefinitivoMunicipio;

	/**
	 * Columna 	P (16)
	 */
	private boolean esNullpresupuestoDefinitivoMunicipio;

	/**
	 * Columna R (18)
	 */
	private boolean esNullcompromisoDefinitivoDepartamento;

	/**
	 * Columna T (21)
	 */
	private boolean esNullPresupuestoDefinitivoDepartamento;

	/**
	 * Columna U (21)
	 */
	private boolean esNullSeguimientoCompromisoPrimerSemMunicipio;

	/**
	 * Columna W (23)
	 */
	private boolean esNullSeguimientoCompromisoSegundoSemMunicipio;

	/**
	 * Columna V (22)
	 */
	private boolean esNullSeguimientoPresupuestoPrimerSemMunicipio;

	/**
	 * Columna X (24)
	 */
	private boolean esNullSeguimientoPresupuestoSegundoSemMunicipio;

	/**
	 * Columna Y (25)
	 */
	private boolean esNullSeguimientoCompromisoPrimerSemDepartamento;

	/**
	 * Columna AA (27)
	 */
	private boolean esNullSeguimientoCompromisoSegundoSemDepartamento;

	/**
	 * Columna (26)
	 */
	private boolean esNullSeguimientoPresupuestoPrimerSemDepartamento;

	/**
	 * Columna AB (28)
	 */
	private boolean esNullSeguimientoPresupuestoSegundoSemDepartamento;

	/**
	 * @return the esNullRespuestaNecesidadPlaneacionMunicipio
	 */
	public boolean isNullRespuestaNecesidadPlaneacionMunicipio() {
		return esNullRespuestaNecesidadPlaneacionMunicipio;
	}

	/**
	 * @param esNullRespuestaNecesidadPlaneacionMunicipio the esNullRespuestaNecesidadPlaneacionMunicipio to set
	 */
	public void setEsNullRespuestaNecesidadPlaneacionMunicipio(boolean esNullRespuestaNecesidadPlaneacionMunicipio) {
		this.esNullRespuestaNecesidadPlaneacionMunicipio = esNullRespuestaNecesidadPlaneacionMunicipio;
	}

	/**
	 * @return the esNullcompromisoDefinitivoMunicipio
	 */
	public boolean isNullcompromisoDefinitivoMunicipio() {
		return esNullcompromisoDefinitivoMunicipio;
	}

	/**
	 * @param esNullcompromisoDefinitivoMunicipio the esNullcompromisoDefinitivoMunicipio to set
	 */
	public void setEsNullcompromisoDefinitivoMunicipio(boolean esNullcompromisoDefinitivoMunicipio) {
		this.esNullcompromisoDefinitivoMunicipio = esNullcompromisoDefinitivoMunicipio;
	}

	/**
	 * @return the esNullpresupuestoDefinitivoMunicipio
	 */
	public boolean isNullpresupuestoDefinitivoMunicipio() {
		return esNullpresupuestoDefinitivoMunicipio;
	}

	/**
	 * @param esNullpresupuestoDefinitivoMunicipio the esNullpresupuestoDefinitivoMunicipio to set
	 */
	public void setEsNullpresupuestoDefinitivoMunicipio(boolean esNullpresupuestoDefinitivoMunicipio) {
		this.esNullpresupuestoDefinitivoMunicipio = esNullpresupuestoDefinitivoMunicipio;
	}

	/**
	 * @return the esNullcompromisoDefinitivoDepartamento
	 */
	public boolean isNullcompromisoDefinitivoDepartamento() {
		return esNullcompromisoDefinitivoDepartamento;
	}

	/**
	 * @param esNullcompromisoDefinitivoDepartamento the esNullcompromisoDefinitivoDepartamento to set
	 */
	public void setEsNullcompromisoDefinitivoDepartamento(boolean esNullcompromisoDefinitivoDepartamento) {
		this.esNullcompromisoDefinitivoDepartamento = esNullcompromisoDefinitivoDepartamento;
	}

	/**
	 * @return the esNullPresupuestoDefinitivoDepartamento
	 */
	public boolean isNullPresupuestoDefinitivoDepartamento() {
		return esNullPresupuestoDefinitivoDepartamento;
	}

	/**
	 * @param esNullPresupuestoDefinitivoDepartamento the esNullPresupuestoDefinitivoDepartamento to set
	 */
	public void setEsNullPresupuestoDefinitivoDepartamento(boolean esNullPresupuestoDefinitivoDepartamento) {
		this.esNullPresupuestoDefinitivoDepartamento = esNullPresupuestoDefinitivoDepartamento;
	}

	/**
	 * @return the esNullSeguimientoCompromisoPrimerSemMunicipio
	 */
	public boolean isNullSeguimientoCompromisoPrimerSemMunicipio() {
		return esNullSeguimientoCompromisoPrimerSemMunicipio;
	}

	/**
	 * @param esNullSeguimientoCompromisoPrimerSemMunicipio the esNullSeguimientoCompromisoPrimerSemMunicipio to set
	 */
	public void setEsNullSeguimientoCompromisoPrimerSemMunicipio(boolean esNullSeguimientoCompromisoPrimerSemMunicipio) {
		this.esNullSeguimientoCompromisoPrimerSemMunicipio = esNullSeguimientoCompromisoPrimerSemMunicipio;
	}

	/**
	 * @return the esNullSeguimientoCompromisoSegundoSemMunicipio
	 */
	public boolean isNullSeguimientoCompromisoSegundoSemMunicipio() {
		return esNullSeguimientoCompromisoSegundoSemMunicipio;
	}

	/**
	 * @param esNullSeguimientoCompromisoSegundoSemMunicipio the esNullSeguimientoCompromisoSegundoSemMunicipio to set
	 */
	public void setEsNullSeguimientoCompromisoSegundoSemMunicipio(boolean esNullSeguimientoCompromisoSegundoSemMunicipio) {
		this.esNullSeguimientoCompromisoSegundoSemMunicipio = esNullSeguimientoCompromisoSegundoSemMunicipio;
	}

	/**
	 * @return the esNullSeguimientoPresupuestoPrimerSemMunicipio
	 */
	public boolean isNullSeguimientoPresupuestoPrimerSemMunicipio() {
		return esNullSeguimientoPresupuestoPrimerSemMunicipio;
	}

	/**
	 * @param esNullSeguimientoPresupuestoPrimerSemMunicipio the esNullSeguimientoPresupuestoPrimerSemMunicipio to set
	 */
	public void setEsNullSeguimientoPresupuestoPrimerSemMunicipio(boolean esNullSeguimientoPresupuestoPrimerSemMunicipio) {
		this.esNullSeguimientoPresupuestoPrimerSemMunicipio = esNullSeguimientoPresupuestoPrimerSemMunicipio;
	}

	/**
	 * @return the esNullSeguimientoPresupuestoSegundoSemMunicipio
	 */
	public boolean isNullSeguimientoPresupuestoSegundoSemMunicipio() {
		return esNullSeguimientoPresupuestoSegundoSemMunicipio;
	}

	/**
	 * @param esNullSeguimientoPresupuestoSegundoSemMunicipio the esNullSeguimientoPresupuestoSegundoSemMunicipio to set
	 */
	public void setEsNullSeguimientoPresupuestoSegundoSemMunicipio(
			boolean esNullSeguimientoPresupuestoSegundoSemMunicipio) {
		this.esNullSeguimientoPresupuestoSegundoSemMunicipio = esNullSeguimientoPresupuestoSegundoSemMunicipio;
	}

	/**
	 * @return the esNullSeguimientoCompromisoPrimerSemDepartamento
	 */
	public boolean isNullSeguimientoCompromisoPrimerSemDepartamento() {
		return esNullSeguimientoCompromisoPrimerSemDepartamento;
	}

	/**
	 * @param esNullSeguimientoCompromisoPrimerSemDepartamento the esNullSeguimientoCompromisoPrimerSemDepartamento to set
	 */
	public void setEsNullSeguimientoCompromisoPrimerSemDepartamento(
			boolean esNullSeguimientoCompromisoPrimerSemDepartamento) {
		this.esNullSeguimientoCompromisoPrimerSemDepartamento = esNullSeguimientoCompromisoPrimerSemDepartamento;
	}

	/**
	 * @return the esNullSeguimientoCompromisoSegundoSemDepartamento
	 */
	public boolean isNullSeguimientoCompromisoSegundoSemDepartamento() {
		return esNullSeguimientoCompromisoSegundoSemDepartamento;
	}

	/**
	 * @param esNullSeguimientoCompromisoSegundoSemDepartamento the esNullSeguimientoCompromisoSegundoSemDepartamento to set
	 */
	public void setEsNullSeguimientoCompromisoSegundoSemDepartamento(
			boolean esNullSeguimientoCompromisoSegundoSemDepartamento) {
		this.esNullSeguimientoCompromisoSegundoSemDepartamento = esNullSeguimientoCompromisoSegundoSemDepartamento;
	}

	/**
	 * @return the esNullSeguimientoPresupuestoPrimerSemDepartamento
	 */
	public boolean isNullSeguimientoPresupuestoPrimerSemDepartamento() {
		return esNullSeguimientoPresupuestoPrimerSemDepartamento;
	}

	/**
	 * @param esNullSeguimientoPresupuestoPrimerSemDepartamento the esNullSeguimientoPresupuestoPrimerSemDepartamento to set
	 */
	public void setEsNullSeguimientoPresupuestoPrimerSemDepartamento(
			boolean esNullSeguimientoPresupuestoPrimerSemDepartamento) {
		this.esNullSeguimientoPresupuestoPrimerSemDepartamento = esNullSeguimientoPresupuestoPrimerSemDepartamento;
	}

	/**
	 * @return the esNullSeguimientoPresupuestoSegundoSemDepartamento
	 */
	public boolean isNullSeguimientoPresupuestoSegundoSemDepartamento() {
		return esNullSeguimientoPresupuestoSegundoSemDepartamento;
	}

	/**
	 * @param esNullSeguimientoPresupuestoSegundoSemDepartamento the esNullSeguimientoPresupuestoSegundoSemDepartamento to set
	 */
	public void setEsNullSeguimientoPresupuestoSegundoSemDepartamento(
			boolean esNullSeguimientoPresupuestoSegundoSemDepartamento) {
		this.esNullSeguimientoPresupuestoSegundoSemDepartamento = esNullSeguimientoPresupuestoSegundoSemDepartamento;
	}
}
