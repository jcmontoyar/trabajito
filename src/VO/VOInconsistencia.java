package VO;

/**
 * Modela un vo de resultados de inconsistencias. Esta ordenado segun la tabla de inconsitencias modelo
 * @author Julian Montoya
 *
 */
public class VOInconsistencia 
{
	/**
	 * Modela array con resultados de inconsistencias
	 */
	private Integer[] resultInconsistencia;

	/**
	 * Modela si hay inconsistencia
	 */
	private Boolean hayInconsistencias;

	/**
	 * Modela si hay inconsistencia
	 */
	private Boolean hayInconsistenciasMunicipParte1;

	/**
	 * Modela si hay inconsistencia
	 */
	private Boolean hayInconsistenciasDeptoParte1;
	
	/**
	 * Modela si hay inconsistencia
	 */
	private Boolean hayInconsistenciasMunicipParte2;

	/**
	 * Modela si hay inconsistencia
	 */
	private Boolean hayInconsistenciasDeptoParte2;

	/**
	 * @return the resultInconsistencia
	 */
	public Integer[] getResultInconsistencia() 
	{
		return resultInconsistencia;
	}

	/**
	 * @param resultInconsistencia the resultInconsistencia to set
	 */
	public void setResultInconsistencia(Integer[] resultInconsistencia)
	{
		this.resultInconsistencia = resultInconsistencia;
	}

	/**
	 * @return the hayInconsistencia
	 */
	public boolean hayInconsistencia()
	{
		if(hayInconsistencias == null)
		{
			for (int i = 0; i < resultInconsistencia.length; i++) 
			{
				if (resultInconsistencia[i] != null && resultInconsistencia[i] > 0)
				{
					hayInconsistencias = true;
					return hayInconsistencias;
				}
			}
			hayInconsistencias = false;
			return hayInconsistencias;
		}
		else
		{
			return hayInconsistencias;
		}
	}

	/**
	 * @return the hayInconsistencia del municipio
	 */
	public boolean hayInconsistenciaMunicipioParte1(int[] pInconsistencias)
	{
		if(hayInconsistenciasMunicipParte1 == null)
		{
			if(!hayInconsistencia())
			{
				hayInconsistenciasMunicipParte1 =  false;
				return hayInconsistenciasMunicipParte1;
			}
			else
			{
				for (int i = 0; i < pInconsistencias.length; i++) 
				{
					if (pInconsistencias[i] > 0)
					{
						hayInconsistenciasMunicipParte1 = true;
						return hayInconsistenciasMunicipParte1;
					}
				}
				hayInconsistenciasMunicipParte1 = false;
				return hayInconsistenciasMunicipParte1;
			}
		}
		else
		{
			return hayInconsistenciasMunicipParte1;
		}
	}

	/**
	 * @return the hayInconsistencia
	 */
	public boolean hayInconsistenciaDelDepartamentoParte1(int[] pInconsistencias)
	{
		if(hayInconsistenciasDeptoParte1 == null)
		{
			if(!hayInconsistencia())
			{
				hayInconsistenciasDeptoParte1 =  false;
				return hayInconsistenciasDeptoParte1;
			}
			else
			{
				for (int i = 0; i < pInconsistencias.length; i++) 
				{
					if (pInconsistencias[i] > 0)
					{
						hayInconsistenciasDeptoParte1 = true;
						return hayInconsistenciasDeptoParte1;
					}
				}
				hayInconsistenciasDeptoParte1 = false;
				return hayInconsistenciasDeptoParte1;
			}
		}
		else
		{
			return hayInconsistenciasDeptoParte1;
		}
	}
	
	/**
	 * @return the hayInconsistencia del municipio
	 */
	public boolean hayInconsistenciaMunicipioParte2(int[] pInconsistencias)
	{
		if(hayInconsistenciasMunicipParte2 == null)
		{
			if(!hayInconsistencia())
			{
				hayInconsistenciasMunicipParte2 =  false;
				return hayInconsistenciasMunicipParte2;
			}
			else
			{
				for (int i = 0; i < pInconsistencias.length; i++) 
				{
					if (pInconsistencias[i] > 0)
					{
						hayInconsistenciasMunicipParte2 = true;
						return hayInconsistenciasMunicipParte2;
					}
				}
				hayInconsistenciasMunicipParte2 = false;
				return hayInconsistenciasMunicipParte2;
			}
		}
		else
		{
			return hayInconsistenciasMunicipParte2;
		}
	}

	/**
	 * @return the hayInconsistencia
	 */
	public boolean hayInconsistenciaDelDepartamentoParte2(int[] pInconsistencias)
	{
		if(hayInconsistenciasDeptoParte2 == null)
		{
			if(!hayInconsistencia())
			{
				hayInconsistenciasDeptoParte2 =  false;
				return hayInconsistenciasDeptoParte2;
			}
			else
			{
				for (int i = 0; i < pInconsistencias.length; i++) 
				{
					if (pInconsistencias[i] > 0)
					{
						hayInconsistenciasDeptoParte2 = true;
						return hayInconsistenciasDeptoParte2;
					}
				}
				hayInconsistenciasDeptoParte2 = false;
				return hayInconsistenciasDeptoParte2;
			}
		}
		else
		{
			return hayInconsistenciasDeptoParte2;
		}
	}
	
	/**
	 * Retorna si hay inconsitencias depto
	 * @return inconsistencias depto
	 */
	public boolean hayInconsistenciasDeptoParte1()
	{
		return hayInconsistenciasDeptoParte1;
	}
	
	/**
	 * Retorna si hay inconsitencias Municip
	 * @return inconsistencias Municip
	 */
	public boolean hayInconsistenciasMunicipParte1()
	{
		return hayInconsistenciasMunicipParte1;
	}
	
	/**
	 * Retorna si hay inconsitencias depto
	 * @return inconsistencias depto
	 */
	public boolean hayInconsistenciasDeptoParte2()
	{
		return hayInconsistenciasDeptoParte2;
	}
	
	/**
	 * Retorna si hay inconsitencias Municip
	 * @return inconsistencias Municip
	 */
	public boolean hayInconsistenciasMunicipParte2()
	{
		return hayInconsistenciasMunicipParte2;
	}
}
