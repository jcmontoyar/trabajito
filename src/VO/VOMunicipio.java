package VO;

import java.util.LinkedHashMap;

/**
 * Modela un municipio, el id se modelar� el las estructuras de datos de la l�gica en la llave del hashMap
 * @author Juli�n Montoya
 */
public class VOMunicipio 
{

	//ATRIBUTOS
	
	/**
	 * Lista llave - valor que modela las preguntas indicativas con sus respectivos datos
	 *  K -> Pregunta indicativa
	 *  V -> Data data (los datos significantes)
	 */
	private LinkedHashMap<String, VOData> data;
	
	//GETTERS AND SETTERS
	
	/**
	 * @return the data
	 */
	public LinkedHashMap<String, VOData> getData() 
	{
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(LinkedHashMap<String, VOData> data)
	{
		this.data = data;
	}
}
