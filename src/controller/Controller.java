package controller;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;

import logic.PAT_Logic;

public class Controller 
{
	/**
	 * Modela la l�gica
	 */
	private static PAT_Logic pat_logic = new PAT_Logic();

	/**
	 * Construir tablas del municipio dado por par�metro
	 * @param municipio
	 * @throws OpenXML4JException 
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public static void crearTablasMunicipio(String municipio) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		pat_logic.construirTablasMunicipio(municipio);
	}
	
	/**
	 * Construye tablas depto
	 * @param depto
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasDepto(String depto) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		pat_logic.construirTablasDepartamento(depto);
	}

	/**
	 * Construye tablas unidas de un conjunto de municipios
	 * @param municipios
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasConjutoMunicipios(String municipios) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		pat_logic.construirTablasConjutoDeMunicipios(municipios);
	}

	/**
	 * Construye tablas del pa�s menos las capitales
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasPaisMenosCapitales() throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		pat_logic.construirTablasPaisMenosCapitales();
	}
	
	/**
	 * Construye tablas del pais (solo capitales)
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasPaisCapitales() throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		pat_logic.construirTablasPaisCapitales();
	}
	
	/**
	 * Construye tablas del pa�s completo
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasPais() throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		pat_logic.construirTablasPais();
	}

	/**
	 * Construye tablas de municipios que llegan por par�metro
	 * @param municipios
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasVariosMunicipios(String municipios) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		pat_logic.creatTablasVariosMunicipios(municipios);
	}

	/**
	 * Construye tablas de departamentos que llegan por par�metro
	 * @param depts
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public static void creatTablasVariosDepartamentos(String depts) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		pat_logic.creatTablasVariosDepartamentos(depts);
	}

	public static void prueba(String prueba) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		pat_logic.prueba(prueba);
		
	}
}
