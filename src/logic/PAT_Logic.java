package logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.SAXException;

import VO.VOData;
import VO.VOInconsistencia;
import VO.VOMunicipio;

public class PAT_Logic
{
	private final static double municipioTotal1 = 0.77;
	private final static double deptoTotal1 = 0.46;
	private final static double totalTotal1 = 0.85;
	private final static double municipioTotal2 = 0.19;
	private final static double deptoTotal2 = 0.05;
	private final static double totalTotal2 = 0.22;

	private final static double municipioNoCapitales1 = 0.77;
	private final static double deptoNoCapitales1 = 0.45;
	private final static double totalTotalNoCapitales1 = 0.85;
	private final static double municipioNoCapitales2 = 0.19;
	private final static double deptoNoCapitales2 = 0.05;
	private final static double totalNoCapitales2 = 0.22;

	private final static double municipiCapitales1 = 0.80;
	private final static double deptoCapitales1 = 0.53;
	private final static double totalTotalCapitales1 = 0.87;
	private final static double municipioCapitales2 = 0.21;
	private final static double deptoCapitales2 = 0.06;
	private final static double totalCapitales2 = 0.25;
	/**
	 * Cadena nombre tabla parte 1
	 */
	private final static String NOMBRE_TABLA_P1 = "Necesidades asociadas al derecho a la";

	/**
	 * Cadena nombre tabla parte 2
	 */
	private final static String NOMBRE_TABLA_P2 = "y compromisos por nivel";

	/**
	 * Modela si ya se cargaron los datos
	 */
	private boolean cargado = false;

	/**
	 *  Estructura de datos
	 */
	private static HashMap<String, HashMap<String, LinkedHashMap<String, VOMunicipio>>> departamenticos;

	/**
	 * Preguntas
	 */
	private static LinkedList<String> preguntas;

	/**
	 * Capitales
	 */
	private static LinkedHashMap<String, String> capitales;

	/**
	 * PAT reader
	 */
	private PAT_Reader patReader;

	public void prueba(String prueba) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			capitales = patReader.capitales;
			cargado = true;
		}
		int cont = 0;
		String[] splited = prueba.split("-");
		for(int i = 0; i < splited.length; i++ )
		{
			for (HashMap<String, LinkedHashMap<String, VOMunicipio>> deptos : departamenticos.values()) 
			{
				if(deptos.containsKey(splited[i]))
				{
					cont++;
				}
			}
			System.out.println(cont + " - " + splited[i] );
			cont = 0;
		}
	}

	/**
	 * Crea las tablas de varios municipios
	 * @param municipios
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public void creatTablasVariosMunicipios(String municipios) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{

		String[] todosLosMunicipiosARealizar = municipios.split("-");
		for (int i = 0; i < todosLosMunicipiosARealizar.length; i++) 
		{
			construirTablasMunicipio(todosLosMunicipiosARealizar[i]);
		}
	}


	/**
	 * Crea las tablas de varios departamentos
	 * @param depts
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public void creatTablasVariosDepartamentos(String depts) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		String[] todosLosDeptsARealizar = depts.split("-");
		for (int i = 0; i < todosLosDeptsARealizar.length; i++) 
		{
			construirTablasDepartamento(todosLosDeptsARealizar[i]);
		}
	}

	/**
	 * Construye las tablas de todo el pa�s consolidado
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public void construirTablasPais() throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			capitales = patReader.capitales;
			cargado = true;
		}

		HashMap<String, LinkedHashMap<String, VOMunicipio>> deptoTemp = new HashMap<>();
		Set<String> departamentosSet = departamenticos.keySet();
		int cont = 0;
		for (String nombreDepto : departamentosSet) 
		{
			HashMap<String, LinkedHashMap<String, VOMunicipio>> departamentoInfo = departamenticos.get(nombreDepto);
			Set<String> municipioSet = departamentoInfo.keySet();
			for (String nombreMunicipio : municipioSet) 
			{
				if(deptoTemp.containsKey(nombreMunicipio))
				{
					String nuevoNombre = nombreMunicipio + cont;
					deptoTemp.put(nuevoNombre, departamentoInfo.get(nombreMunicipio));
					cont++;
				}
				else
				{
					deptoTemp.put(nombreMunicipio, departamentoInfo.get(nombreMunicipio));	
				}
			}
		}
		System.out.println(deptoTemp.size());
		departamenticos.put("Total Pa�s", deptoTemp);
		construirTablasDepartamento("Total Pa�s");
		departamenticos.remove("Total Pa�s");

	}

	/**
	 * Construye tablas del conjunto de todas las capitales de los departamentos
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public void construirTablasPaisCapitales() throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			capitales = patReader.capitales;
			cargado = true;
		}

		HashMap<String, LinkedHashMap<String, VOMunicipio>> deptoTemp = new HashMap<>();
		Set<String> departamentosSet = departamenticos.keySet();
		for (String nombreDepto : departamentosSet) 
		{
			if(!nombreDepto.equals("Cundinamarca"))
			{

				HashMap<String, LinkedHashMap<String, VOMunicipio>> departamentoInfo = departamenticos.get(nombreDepto);
				deptoTemp.put(capitales.get(nombreDepto), departamentoInfo.get(capitales.get(nombreDepto)));
			}
		}
		departamenticos.put("TotalCapitales", deptoTemp);
		construirTablasDepartamento("TotalCapitales");
		departamenticos.remove("TotalCapitales");

	}

	public void construirTablasPaisMenosCapitales() throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			capitales = patReader.capitales;
			cargado = true;
		}

		int cont = 0;
		HashMap<String, LinkedHashMap<String, VOMunicipio>> deptoTemp = new HashMap<>();
		Set<String> departamentosSet = departamenticos.keySet();
		for (String nombreDepto : departamentosSet) 
		{
			HashMap<String, LinkedHashMap<String, VOMunicipio>> departamentoInfo = departamenticos.get(nombreDepto);
			Set<String> municipioSet = departamentoInfo.keySet();
			for (String nombreMunicipio : municipioSet) 
			{
				if(nombreDepto.equals("Cundinamarca") ||!capitales.get(nombreDepto).equals(nombreMunicipio))
				{
					if(deptoTemp.containsKey(nombreMunicipio))
					{
						String nuevoNombre = nombreMunicipio + cont;
						deptoTemp.put(nuevoNombre, departamentoInfo.get(nombreMunicipio));
						cont++;
					}
					else
					{
						deptoTemp.put(nombreMunicipio, departamentoInfo.get(nombreMunicipio));	
					}
				}
			}
		}
		departamenticos.put("Total_SinCapitales", deptoTemp);
		construirTablasDepartamento("Total_SinCapitales");
		departamenticos.remove("Total_SinCapitales");
	}

	/**
	 * Construye las tablas de un conjunto de municipios
	 * @param municipios
	 * @throws OpenXML4JException 
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public void construirTablasConjutoDeMunicipios(String municipios) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException 
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			cargado = true;
		}
		String[] municips = municipios.split("-");
		HashMap<String, LinkedHashMap<String, VOMunicipio>> deptoTemp = new HashMap<>();
		for (int i = 0; i < municips.length; i++)
		{
			LinkedHashMap<String, VOMunicipio> municipioData = null;
			for (HashMap<String, LinkedHashMap<String, VOMunicipio>> hashito : departamenticos.values()) 
			{
				if(hashito.containsKey(municips[i]))
				{
					municipioData = hashito.get(municips[i]);
					break;
				}
			}
			deptoTemp.put(municips[i], municipioData);
		}
		departamenticos.put(municipios, deptoTemp);
		construirTablasDepartamento(municipios);
		departamenticos.remove(municipios);
	}

	/**
	 * Construye las tablas de un depto
	 * @param departamento
	 * @throws OpenXML4JException 
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public void construirTablasDepartamento(String departamento) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			cargado = true;
		}
		LinkedHashMap<String, VOMunicipio> deptoData = construirDeptoData(departamento);
		construirSheets(deptoData, departamento, departamenticos.get(departamento).size(), true);
	}

	/**
	 * Construye la data sumada para el departamento
	 * @param departamento
	 */
	private LinkedHashMap<String, VOMunicipio> construirDeptoData(String departamento)
	{
		LinkedHashMap<String, VOMunicipio> deptoData = new LinkedHashMap<>();
		if(departamenticos.containsKey(departamento))
		{
			//Info depto raw
			HashMap<String, LinkedHashMap<String, VOMunicipio>> deptoRaw = departamenticos.get(departamento);

			//Iterar sobre las pregutas
			for (String preguntaInit : preguntas) 
			{
				VOData dataSumatoriaPregunta = null;
				StringBuilder stringBuilderDerecho = null;

				//Collection municipios
				Collection<LinkedHashMap<String, VOMunicipio>> setMunicipio = deptoRaw.values();
				for (LinkedHashMap<String, VOMunicipio> municip : setMunicipio) 
				{
					Set<String> setDerechos = municip.keySet();
					for (String stringDerecho : setDerechos) 
					{
						if(municip.get(stringDerecho).getData().containsKey(preguntaInit))
						{
							VOData aSumar = municip.get(stringDerecho).getData().get(preguntaInit);
							if(dataSumatoriaPregunta == null)
							{
								dataSumatoriaPregunta = aSumar;
								stringBuilderDerecho = new StringBuilder(stringDerecho);
							}
							else
							{
								dataSumatoriaPregunta = dataSumatoriaPregunta.sumarVOData(aSumar);
							}
							break;

						}
					}
				}			
				if(!deptoData.containsKey(stringBuilderDerecho.toString()))
				{
					VOMunicipio addVo = new VOMunicipio();
					LinkedHashMap<String, VOData> addListMun = new LinkedHashMap<>();
					addListMun.put(preguntaInit, dataSumatoriaPregunta);
					addVo.setData(addListMun);
					deptoData.put(stringBuilderDerecho.toString(), addVo);
				}
				else
				{
					deptoData.get(stringBuilderDerecho.toString()).getData().put(preguntaInit, dataSumatoriaPregunta);
				}	
			}

			//Calcular el total de cada derecho
			calcularTotales(deptoData, deptoRaw);
		}
		else
		{
			return null;
		}
		return deptoData;
	}

	/**
	 * Calcula los totales por cada derecho
	 * @param deptoData
	 * @param deptoRaw
	 */
	private void calcularTotales(LinkedHashMap<String, VOMunicipio> deptoData, HashMap<String, LinkedHashMap<String, VOMunicipio>> deptoRaw) 
	{
		Set<String> setDerechos = deptoData.keySet();
		for (String str : setDerechos) 
		{
			VOData dataSumatoriaPregunta = null;

			//Collection municipios
			Collection<LinkedHashMap<String, VOMunicipio>> setMunicipio = deptoRaw.values();
			for (LinkedHashMap<String, VOMunicipio> municip : setMunicipio) 
			{
				VOData aSumar = municip.get(str).getData().get("Total");
				if(dataSumatoriaPregunta == null)
				{
					dataSumatoriaPregunta = aSumar;
				}
				else
				{
					dataSumatoriaPregunta = dataSumatoriaPregunta.sumarVOData(aSumar);
				}
			}
			deptoData.get(str).getData().put("Total", dataSumatoriaPregunta);
		}
	}

	/**
	 * Construye las tablas del municipio dado por par�metro
	 * @param municipio
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public void construirTablasMunicipio(String municipio) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		if(!cargado)
		{
			patReader = new PAT_Reader();
			patReader.cargarDatos(PAT_Reader.FILE_PAT);
			departamenticos = patReader.getDepartamentos();
			preguntas = patReader.preguntasIndicativas;
			cargado = true;
		}
		LinkedHashMap<String, VOMunicipio> municipioData = null;
		if(municipio.equals("Rionegro"))
		{
			municipioData = departamenticos.get("Santader").get("Rionegro");
		}
		else if(municipio.equals("El Tambo"))
		{
			municipioData = departamenticos.get("Cauca").get("El Tambo");
		}
		else
		{
			{

				for (HashMap<String, LinkedHashMap<String, VOMunicipio>> hashito : departamenticos.values()) 
				{
					if(hashito.containsKey(municipio))
					{
						municipioData = hashito.get(municipio);
						break;
					}
				}
			}
		}
		//construir sheets
		construirSheets(municipioData,  municipio, 1, false);
	}

	/**
	 * Construye las sheets y la data
	 * @param municipioData (info a ser imprimida)
	 */
	private void construirSheets(LinkedHashMap<String, VOMunicipio> municipioData, String nombreFile, int numeroMunicipios, boolean esDepartamento)
	{
		//Crear workbook
		XSSFWorkbook workbook = new XSSFWorkbook();
		Set<String> setDerechos = municipioData.keySet();

		//VOMunicipio total
		VOMunicipio voTotal = new VOMunicipio();
		LinkedHashMap<String, VOData> dataTotal= new LinkedHashMap<>();
		voTotal.setData(dataTotal);
		XSSFSheet sheetTotal = workbook.createSheet(nombreFile + " - Consolidado");
		System.out.println("Creando tablas por derecho....");
		for (String derecho : setDerechos) 
		{
			XSSFSheet sheet = workbook.createSheet(nombreFile + " - " + derecho);
			inicializarSheet(sheet, NOMBRE_TABLA_P1 + " " + derecho + " " + NOMBRE_TABLA_P2);

			//Construir tabla base
			construirTablaBase(sheet, false);

			llenarInformacion(sheet, municipioData.get(derecho), derecho, voTotal, false);
			ajustarTamanios(sheet, false);
		}

		//Crear sheet consolidado y asignar valores
		System.out.println("Creando tabla resultado consolidado.....");
		inicializarSheet(sheetTotal, "Consolidado");

		//Construir tabla base
		construirTablaBase(sheetTotal, true);

		//Llenar tabla con info
		llenarInformacion(sheetTotal, voTotal, null, null, true);

		//Ajustar algunos tama�os
		ajustarTamanios(sheetTotal, true);

		System.out.println("Creando tabla de inconsistencias......");
		creatSheetInconsistencias(workbook, nombreFile, municipioData, numeroMunicipios, esDepartamento);
		try 
		{
			FileOutputStream out = new FileOutputStream(new File("./docs/results/"+nombreFile+".xlsx"));
			workbook.write(out);
			out.close();
			System.out.println("Archivo correctamente creado");
			workbook.close();
		}
		catch (IOException e) 
		{

		}
	}

	/**
	 * Crea la sheet de inconsistencias
	 * @param workbook
	 * @param municipioData 
	 */
	private void creatSheetInconsistencias(XSSFWorkbook workbook, String nombreFile, LinkedHashMap<String, VOMunicipio> municipioData, int numeroMunicipios, boolean esDepartamento) 
	{
		XSSFSheet sheet = workbook.createSheet(nombreFile + " - Inconsistencias");
		Row row = sheet.createRow(0);
		Cell cell1 = row.createCell(0);
		cell1.setCellValue("Inconsistencias tablero PAT. " + nombreFile);

		//Construir tabla base
		construirBaseTablaInconsistencias(sheet);

		//Llenar con data
		if(!esDepartamento)
		{
			llenarTablaInconsistenciasMunicipio(sheet, municipioData, numeroMunicipios);
		}
		else
		{
			llenarTablaInconsistensiasDepartamento(sheet, numeroMunicipios, nombreFile);
		}
		arreglarColsTablaInconsistencias(sheet);
	}

	/**
	 * Arregla las cols de la sheet de inconsistencias
	 * @param sheet
	 */
	private void arreglarColsTablaInconsistencias(XSSFSheet sheet) 
	{
		sheet.setColumnWidth(0, 2700);
		sheet.autoSizeColumn(1, true);
		sheet.setColumnWidth(2, 11880);
		sheet.autoSizeColumn(3, true);
		sheet.autoSizeColumn(4, true);
		sheet.autoSizeColumn(5, true);
	}

	private void llenarTablaInconsistensiasDepartamento(XSSFSheet sheet, int numeroMunicipios, String nombreFile) 
	{
		Integer[] resultadosInconsitencias = null;
		int contadorPreguntaConInconsistenciaParte1 = 0;
		int contadorPreguntaConInconsistenciaParte2 = 0;
		int contadorPreguntaProblemaMunicipioParte1 = 0;
		int contadorPreguntaProblemaDeptoParte1 = 0;
		int contadorPreguntaProblemaMunicipioParte2 = 0;
		int contadorPreguntaProblemaDeptoParte2 = 0;
		HashMap<String, LinkedHashMap<String, VOMunicipio>> depto = departamenticos.get(nombreFile);
		Collection<LinkedHashMap<String, VOMunicipio>> deptoValues = depto.values();
		for(LinkedHashMap<String, VOMunicipio> municipioData: deptoValues)
		{
			Collection<VOMunicipio> vos = municipioData.values();
			for (VOMunicipio vo : vos) 
			{
				LinkedHashMap<String, VOData> listData = vo.getData();
				Set<String> set = listData.keySet();

				for(String pregunta  : set)
				{
					if(!pregunta.equals("Total"))
					{
						VOInconsistencia tempInconsistencias = listData.get(pregunta).calcularInconsistencias();
						if(tempInconsistencias.hayInconsistencia())
						{
							if(tempInconsistencias.hayInconsistenciasDeptoParte1() || tempInconsistencias.hayInconsistenciasMunicipParte1())
							{
								contadorPreguntaConInconsistenciaParte1++;
								if(tempInconsistencias.hayInconsistenciasDeptoParte1())
								{
									contadorPreguntaProblemaDeptoParte1++;
								}
								if(tempInconsistencias.hayInconsistenciasMunicipParte1())
								{
									contadorPreguntaProblemaMunicipioParte1++;
								}
							}
							if(tempInconsistencias.hayInconsistenciasDeptoParte2() || tempInconsistencias.hayInconsistenciasMunicipParte2())
							{
								contadorPreguntaConInconsistenciaParte2++;
								if(tempInconsistencias.hayInconsistenciasDeptoParte2())
								{
									contadorPreguntaProblemaDeptoParte2++;
								}
								if(tempInconsistencias.hayInconsistenciasMunicipParte2())
								{
									contadorPreguntaProblemaMunicipioParte2++;
								}
							}
							if(resultadosInconsitencias == null)
							{
								resultadosInconsitencias = tempInconsistencias.getResultInconsistencia();
							}
							else
							{
								resultadosInconsitencias = sumarArraysInteger(resultadosInconsitencias, tempInconsistencias.getResultInconsistencia());
							}
						}
					}
				}
			}
		}
		resultadosInconsitencias[21] = contadorPreguntaProblemaMunicipioParte1;
		resultadosInconsitencias[22] = contadorPreguntaProblemaDeptoParte1;
		resultadosInconsitencias[23] = contadorPreguntaConInconsistenciaParte1;
		resultadosInconsitencias[32] = contadorPreguntaProblemaMunicipioParte2;
		resultadosInconsitencias[33] = contadorPreguntaProblemaDeptoParte2;
		resultadosInconsitencias[34] = contadorPreguntaConInconsistenciaParte2;

		imprimirInconsistencias(sheet, resultadosInconsitencias, numeroMunicipios);	
	}

	/**
	 * Llena la tabla de inconsistencias con la data
	 * @param sheet de inconsistencias
	 * @param municipioData 
	 */
	private void llenarTablaInconsistenciasMunicipio(XSSFSheet sheet, LinkedHashMap<String, VOMunicipio> municipioData, int numeroMunicipios)
	{	
		Integer[] resultadosInconsitencias = null; 
		int contadorPreguntaConInconsistenciaParte1 = 0;
		int contadorPreguntaConInconsistenciaParte2 = 0;
		int contadorPreguntaProblemaMunicipioParte1 = 0;
		int contadorPreguntaProblemaDeptoParte1 = 0;
		int contadorPreguntaProblemaMunicipioParte2 = 0;
		int contadorPreguntaProblemaDeptoParte2 = 0;
		Collection<VOMunicipio> vos = municipioData.values();
		for (VOMunicipio vo : vos) 
		{
			LinkedHashMap<String, VOData> listData = vo.getData();
			Set<String> set = listData.keySet();

			for(String pregunta  : set)
			{
				if(!pregunta.equals("Total"))
				{
					VOInconsistencia tempInconsistencias = listData.get(pregunta).calcularInconsistencias();
					if(tempInconsistencias.hayInconsistencia())
					{
						if(tempInconsistencias.hayInconsistenciasDeptoParte1() || tempInconsistencias.hayInconsistenciasMunicipParte1())
						{
							contadorPreguntaConInconsistenciaParte1++;
							if(tempInconsistencias.hayInconsistenciasDeptoParte1())
							{
								contadorPreguntaProblemaDeptoParte1++;
							}
							if(tempInconsistencias.hayInconsistenciasMunicipParte1())
							{
								contadorPreguntaProblemaMunicipioParte1++;
							}
						}
						if(tempInconsistencias.hayInconsistenciasDeptoParte2() || tempInconsistencias.hayInconsistenciasMunicipParte2())
						{
							contadorPreguntaConInconsistenciaParte2++;
							if(tempInconsistencias.hayInconsistenciasDeptoParte2())
							{
								contadorPreguntaProblemaDeptoParte2++;
							}
							if(tempInconsistencias.hayInconsistenciasMunicipParte2())
							{
								contadorPreguntaProblemaMunicipioParte2++;
							}
						}
						if(resultadosInconsitencias == null)
						{
							resultadosInconsitencias = tempInconsistencias.getResultInconsistencia();
						}
						else
						{
							resultadosInconsitencias = sumarArraysInteger(resultadosInconsitencias, tempInconsistencias.getResultInconsistencia());
						}
					}
				}
			}
		}
		resultadosInconsitencias[21] = contadorPreguntaProblemaMunicipioParte1;
		resultadosInconsitencias[22] = contadorPreguntaProblemaDeptoParte1;
		resultadosInconsitencias[23] = contadorPreguntaConInconsistenciaParte1;
		resultadosInconsitencias[32] = contadorPreguntaProblemaMunicipioParte2;
		resultadosInconsitencias[33] = contadorPreguntaProblemaDeptoParte2;
		resultadosInconsitencias[34] = contadorPreguntaConInconsistenciaParte2;

		imprimirInconsistencias(sheet, resultadosInconsitencias, numeroMunicipios);
	}

	/**
	 * Imprime las inconsitencias
	 * @param sheet
	 * @param resultadosInconsitencias
	 */
	private void imprimirInconsistencias(XSSFSheet sheet, Integer[] resultadosInconsitencias, int numeroMunicipios)
	{
		for (int i = 0; i < resultadosInconsitencias.length; i++) 
		{
			int denominador = preguntas.size();
			int numeroRow = 0;
			if(i < 24)
			{
				numeroRow = i + 2;
			}
			else
			{
				numeroRow = i + 7;
			}
			if(i == 15 || i == 16 || i == 19 || i == 20)
			{
				denominador = denominador*2;
			}
			setBordersToCellsEInicializarCeldasDeInconsistencia(sheet, numeroRow, 3, resultadosInconsitencias[i], denominador*numeroMunicipios);
		}

	}

	/**
	 * Suma arrays de inconsistencias
	 * @param array1
	 * @param array2
	 * @return array suma
	 */
	private Integer[] sumarArraysInteger(Integer[] array1, Integer[] array2)
	{
		Integer[] ret = new Integer[array1.length];
		for (int i = 0; i < array1.length; i++) 
		{
			Integer valor1 = array1[i];
			Integer valor2 = array2[i];
			if(valor1 != null && valor2 != null)
			{
				ret[i] = valor1 + valor2;
			}
		}
		return ret;
	}

	/**
	 * Construye la table base para las inconsistencias
	 * @param sheet
	 */
	private void construirBaseTablaInconsistencias(XSSFSheet sheet) 
	{
		//Cell de fase
		CellRangeAddress rangeAddress = new CellRangeAddress(1,1,0,0);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Fase", false, false, true);

		//Cell de fase
		rangeAddress = new CellRangeAddress(1,1,1,1);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "�mbito", false, false, true);

		//Cell inconsistencia / deficiencia
		rangeAddress = new CellRangeAddress(1,1,2,2);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Inconsistencia / deficiencia de reporte", false, false, true);

		//Cell casos
		rangeAddress = new CellRangeAddress(1,1,3,3);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Casos", false, false, true);

		// %
		rangeAddress = new CellRangeAddress(1,1,4,4);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "%", false, false, true);

		// En la fase de planeacion
		rangeAddress = new CellRangeAddress(2,14,0,0);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "En la fase de planeaci�n", true, true, true);

		// En la fase de ejecuci�n
		rangeAddress = new CellRangeAddress(15,22,0,0);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "En la fase de ejecuci�n", true, true, true);

		// #preguntas con algun problema muni
		rangeAddress = new CellRangeAddress(23,23,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del municipio", true, false, true);

		// #preguntas con algun problema depto
		rangeAddress = new CellRangeAddress(24,24,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del departamento", true, false, true);

		// #preguntas con algun problema depto
		rangeAddress = new CellRangeAddress(25,25,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia (departamento o municipio)", true, false, true);

		//Municipio
		rangeAddress = new CellRangeAddress(2,8,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Municipio", true, false, true);

		//Depto
		rangeAddress = new CellRangeAddress(9,14,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Departamento", true, false, true);

		//Municipio
		rangeAddress = new CellRangeAddress(15,18,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Municipio", true, false, true);

		//Depto
		rangeAddress = new CellRangeAddress(19,22,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Departamento", true, false, true);

		//1
		rangeAddress = new CellRangeAddress(2,2,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso cuando no se reporta necesidad", false, false, false);

		//2
		rangeAddress = new CellRangeAddress(3,3,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso superior a necesidad", false, false, false);

		//3
		rangeAddress = new CellRangeAddress(4,4,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso sin presupuesto", false, false, false);

		//4
		rangeAddress = new CellRangeAddress(5,5,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin compromiso", false, false, false);

		//5
		rangeAddress = new CellRangeAddress(6,6,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Necesidad sin diligenciar", false, false, false);

		//6
		rangeAddress = new CellRangeAddress(7,7,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso sin diligenciar", false, false, false);

		//7
		rangeAddress = new CellRangeAddress(8,8,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin diligenciar", false, false, false);

		//1
		rangeAddress = new CellRangeAddress(9,9,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso cuando no se reporta necesidad", false, false, false);

		//2
		rangeAddress = new CellRangeAddress(10,10,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso superior a necesidad", false, false, false);

		//3
		rangeAddress = new CellRangeAddress(11,11,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso sin presupuesto", false, false, false);

		//4
		rangeAddress = new CellRangeAddress(12,12,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin compromiso", false, false, false);

		//6
		rangeAddress = new CellRangeAddress(13,13,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Compromiso sin diligenciar a pesar de existir necesidad", false, false, false);

		//7
		rangeAddress = new CellRangeAddress(14,14,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin diligenciar a pesar de existir necesidad", false, false, false);

		//1
		rangeAddress = new CellRangeAddress(15,15,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ejecuci�n f�sica sin presupuesto", false, false, false);

		//2
		rangeAddress = new CellRangeAddress(16,16,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin ejecuci�n f�sica", false, false, false);

		//3
		rangeAddress = new CellRangeAddress(17,17,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ejecuci�n f�sica sin diligenciar", false, false, false);

		//4
		rangeAddress = new CellRangeAddress(18,18,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin diligenciar", false, false, false);

		//1
		rangeAddress = new CellRangeAddress(19,19,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ejecuci�n f�sica sin presupuesto", false, false, false);

		//2
		rangeAddress = new CellRangeAddress(20,20,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin ejecuci�n f�sica", false, false, false);

		//3
		rangeAddress = new CellRangeAddress(21,21,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ejecuci�n f�sica sin diligenciar", false, false, false);

		//4
		rangeAddress = new CellRangeAddress(22,22,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Presupuesto sin diligenciar", false, false, false);

		//Otro titulo
		Row row = sheet.createRow(29);
		Cell cellTit = row.createCell(0);
		cellTit.setCellValue("Inconsistencias entre la fase de planeaci�n y ejecuci�n ");

		//Cell de fase
		rangeAddress = new CellRangeAddress(30,30,0,0);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "�mbito", false, false, true);

		//Cell de fase
		rangeAddress = new CellRangeAddress(30,30,1,1);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Asunto", false, false, true);

		//Cell inconsistencia / deficiencia
		rangeAddress = new CellRangeAddress(30,30,2,2);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Descripci�n", false, false, true);

		//Cell casos
		rangeAddress = new CellRangeAddress(30,30,3,3);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Casos", false, false, true);

		// %
		rangeAddress = new CellRangeAddress(30,30,4,4);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "%", false, false, true);

		//Municipio
		rangeAddress = new CellRangeAddress(31,34,0,0);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Municipio", true, false, true);

		//Depto
		rangeAddress = new CellRangeAddress(35,38,0,0);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Departamento", true, false, true);

		// #preguntas con algun problema muni
		rangeAddress = new CellRangeAddress(39,39,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del municipio", true, false, true);

		// #preguntas con algun problema depto
		rangeAddress = new CellRangeAddress(40,40,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del departamento", true, false, true);

		// #preguntas con algun problema
		rangeAddress = new CellRangeAddress(41,41,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia (departamento o municipio)", true, false, true);

		//Valor por unidad
		rangeAddress = new CellRangeAddress(31,32,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Valor por unidad", true, false, true);

		//No correspondencia
		rangeAddress = new CellRangeAddress(33,34,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "No correspondencia", true, false, true);

		//Valor por unidad
		rangeAddress = new CellRangeAddress(35,36,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Valor por unidad", true, false, true);

		//No correspondencia
		rangeAddress = new CellRangeAddress(37,38,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "No correspondencia", true, false, true);

		//1
		rangeAddress = new CellRangeAddress(31,31,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ostensiblemente mayor en la ejecuci�n", false, false, false);

		//2
		rangeAddress = new CellRangeAddress(32,32,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ostensiblemente inferior en la ejecuci�n", false, false, false);

		//3
		rangeAddress = new CellRangeAddress(33,33,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Se ejecuta, pero no habia planificado", false, false, false);

		//4
		rangeAddress = new CellRangeAddress(34,34,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Se planific�, pero no se ejecut�", false, false, false);

		//1
		rangeAddress = new CellRangeAddress(35,35,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ostensiblemente mayor en la ejecuci�n", false, false, false);

		//2
		rangeAddress = new CellRangeAddress(36,36,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Ostensiblemente inferior en la ejecuci�n", false, false, false);

		//3
		rangeAddress = new CellRangeAddress(37,37,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Se ejecuta, pero no habia planificado", false, false, false);

		//4
		rangeAddress = new CellRangeAddress(38,38,2,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "Se planific�, pero no se ejecut�", false, false, false);

		//Datos referencia
		row = sheet.createRow(44);
		cellTit = row.createCell(0);
		cellTit.setCellValue("Datos de referencia");

		// Inconsistencias tablero PAT (fases de planeaci�n y ejecuci�n)
		rangeAddress = new CellRangeAddress(45,45,0,2);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Inconsistencias tablero PAT (fases de planeaci�n y ejecuci�n)", true, false, true);

		// Total nacional
		rangeAddress = new CellRangeAddress(45,45,3,3);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Total nacional", false, false, true);

		// Capitales
		rangeAddress = new CellRangeAddress(45,45,4,4);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Capitales", false, false, true);

		// No capitales
		rangeAddress = new CellRangeAddress(45,45,5,5);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "No capitales", false, false, true);	

		// #preguntas con algun problema muni
		rangeAddress = new CellRangeAddress(46,46,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del municipio", true, false, true);

		// #preguntas con algun problema depto
		rangeAddress = new CellRangeAddress(47,47,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del departamento", true, false, true);

		// #preguntas con algun problema
		rangeAddress = new CellRangeAddress(48,48,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia (departamento o municipio)", true, false, true);


		// #preguntas con algun problema muni
		rangeAddress = new CellRangeAddress(52,52,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del municipio", true, false, true);

		// #preguntas con algun problema depto
		rangeAddress = new CellRangeAddress(53,53,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia por parte del departamento", true, false, true);

		// #preguntas con algun problema
		rangeAddress = new CellRangeAddress(54,54,0,2);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, "N�mero de preguntas con alguna inconsistencia (departamento o municipio)", true, false, true);


		// Inconsistencias Tablero PAT (entre la fase de planeaci�n y ejecuci�n )
		rangeAddress = new CellRangeAddress(51,51,0,2);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Inconsistencias Tablero PAT (entre la fase de planeaci�n y ejecuci�n)", true, false, true);

		// Total nacional
		rangeAddress = new CellRangeAddress(51,51,3,3);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Total nacional", false, false, true);

		// Capitales
		rangeAddress = new CellRangeAddress(51,51,4,4);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Capitales", false, false, true);

		// No capitales
		rangeAddress = new CellRangeAddress(51,51,5,5);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "No capitales", false, false, true);

		// Total muni
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 46, 3, new BigDecimal(municipioTotal1), true, false);

		// Total depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 47, 3, new BigDecimal(deptoTotal1), true, false);

		// Total total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 48, 3, new BigDecimal(totalTotal1), true, false);

		// capitales muni
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 46, 4, new BigDecimal(municipiCapitales1), true, false);

		// capitales depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 47, 4, new BigDecimal(deptoCapitales1), true, false);

		// capitales total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 48, 4, new BigDecimal(totalTotalCapitales1), true, false);

		// no capitales muni
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 46, 5, new BigDecimal(municipioNoCapitales1), true, false);

		// no capitales depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 47, 5, new BigDecimal(deptoNoCapitales1), true, false);

		// no capitales total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 48, 5, new BigDecimal(totalTotalNoCapitales1), true, false);



		// Total muni
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 52, 3, new BigDecimal(municipioTotal2), true, false);

		// Total depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 53, 3, new BigDecimal(deptoTotal2), true, false);

		// Total total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 54, 3, new BigDecimal(totalTotal2), true, false);

		// capitales muni
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 52, 4, new BigDecimal(municipioCapitales2), true, false);

		// capitales depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 53, 4, new BigDecimal(deptoCapitales2), true, false);

		// capitales total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 54, 4, new BigDecimal(totalCapitales2), true, false);

		// no capitales muni
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 52, 5, new BigDecimal(municipioNoCapitales2), true, false);

		// no capitales depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 53, 5, new BigDecimal(deptoNoCapitales2), true, false);

		// no capitales total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, 54, 5, new BigDecimal(totalNoCapitales2), true, false);
	}

	/**
	 * Ajusta tamanios de las columnas y filas
	 * @param sheet
	 */
	private void ajustarTamanios(XSSFSheet sheet, boolean consolidado)
	{
		if(consolidado)
		{
			sheet.setColumnWidth(1, 8000);
		}
		else
		{
			sheet.setColumnWidth(1, 10000);
		}
		sheet.getRow(3).setHeightInPoints(30);
		for (int i = 2; i < 18; i++) 
		{
			sheet.autoSizeColumn(i, true);
		}
		sheet.setColumnWidth(7, 2500);
	}

	/**
	 * Inicializa la sheet con el t�tulo de la tabla
	 * @param sheet
	 * @param tituloTabla
	 */
	private void inicializarSheet(XSSFSheet sheet, String tituloTabla)
	{
		CellStyle style = sheet.getWorkbook().createCellStyle();
		int rownum = 1;
		Row row = sheet.createRow(rownum);
		Cell cell1 = row.createCell(1);
		cell1.setCellValue(tituloTabla);
		Font newFont = sheet.getWorkbook().createFont();

		newFont.setBold(true);
		newFont.setFontHeight((short)(9*20));
		style.setFont(newFont);
		cell1.setCellStyle(style);

	}

	/**
	 * Llena la tabla con los datos y va asignando al VOMunicipio total que modela el consolidado (la data tendr� todos los totales de cada derecho)
	 * @param sheet
	 * @param voMunicipio
	 */
	private void llenarInformacion(XSSFSheet sheet, VOMunicipio voMunicipio, String derecho, VOMunicipio voTotal, boolean esElTotal)
	{	
		VOData dataTotalDelTotal = null;
		LinkedHashMap<String, VOData> data = voMunicipio.getData();
		Set<String> set = data.keySet();
		int rowNum = 6;
		for (String pregunta : set) 
		{
			boolean boldTotal = false;
			sheet.createRow(rowNum);

			//data object
			VOData voData = data.get(pregunta);
			if(pregunta.equals("Total"))
			{
				boldTotal = true;
			}

			//imprimir data en el excel
			printEnExcelData(sheet, rowNum, voData, pregunta, boldTotal);
			rowNum++;

			if(pregunta.equals("Total") && !esElTotal)
			{
				voTotal.getData().put(derecho, voData);
			}
			else if (esElTotal)
			{
				if(dataTotalDelTotal == null)
				{
					dataTotalDelTotal = voData;
				}
				else
				{
					dataTotalDelTotal = dataTotalDelTotal.sumarVOData(voData);
				}
				if(rowNum == 6 + set.size())
				{
					printEnExcelData(sheet, rowNum, dataTotalDelTotal, "Total", true);
				}
			}
		}
	}

	/**
	 * Construye la tabla base
	 * @param sheet donde se har� la tabla
	 */
	private void construirTablaBase(XSSFSheet sheet, boolean consolidado)
	{
		//Cell de preguntas
		CellRangeAddress rangeAddress = new CellRangeAddress(3,5,1,1);
		if(consolidado)
		{
			setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Derecho", true, false, true);
		}
		else
		{
			setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Preguntas", true, false, true);
		}

		//Cell necesidad
		rangeAddress = new CellRangeAddress(3,5,2,2);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Necesidad", true, false, true);

		//Cell compromisos
		rangeAddress = new CellRangeAddress(3,3,3,6);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Compromisos", true, false, true);

		//Cell municipio
		rangeAddress = new CellRangeAddress(4,4,3,4);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Municipio", true, false, true);

		//Cell departamento
		rangeAddress = new CellRangeAddress(4,4,5,6);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Departamento", true, false, true);

		//Cell numero
		rangeAddress = new CellRangeAddress(5,5,3,3);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "N�mero", false, false, true);

		//Cell presupuesto
		rangeAddress = new CellRangeAddress(5,5,4,4);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Presupuesto", false, false, true);

		//Cell numero
		rangeAddress = new CellRangeAddress(5,5,5,5);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "N�mero", false, false, true);

		//Cell presupuesto
		rangeAddress = new CellRangeAddress(5,5,6,6);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Presupuesto", false, false, true);

		//Cell %necesidades cubiertas con compromiso
		rangeAddress = new CellRangeAddress(3,5,7,7);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "% necesidades cubiertas con compromiso", true, true, true);

		//Cell Ejecuci�n
		rangeAddress = new CellRangeAddress(3,3,8,11);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Ejecuci�n", true, false, true);

		//Cell municipio
		rangeAddress = new CellRangeAddress(4,4,8,9);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Municipio", true, false, true);

		//Cell departamento
		rangeAddress = new CellRangeAddress(4,4,10,11);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Departamento", true, false, true);

		//Cell numero
		rangeAddress = new CellRangeAddress(5,5,8,8);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "N�mero", false, false, true);

		//Cell presupuesto
		rangeAddress = new CellRangeAddress(5,5,9,9);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Presupuesto", false, false, true);

		//Cell numero
		rangeAddress = new CellRangeAddress(5,5,10,10);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "N�mero", false, false, true);

		//Cell presupuesto
		rangeAddress = new CellRangeAddress(5,5,11,11);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Presupuesto", false, false, true);

		//Cell %necesidades cubiertas con ejecuci�n
		rangeAddress = new CellRangeAddress(3,3,12,14);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "% necesidades cubiertas con ejecuci�n", true, true, true);

		//Cell municipio
		rangeAddress = new CellRangeAddress(4,5,12,12);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Municipio", true, false, true);

		//Cell depto
		rangeAddress = new CellRangeAddress(4,5,13,13);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Depto", true, false, true);

		//Cell total
		rangeAddress = new CellRangeAddress(4,5,14,14);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Total", true, false, true);

		//% ejecuci�n/ compromisos
		rangeAddress = new CellRangeAddress(3,3,15,17);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "% Ejecuci�n/compromisos ($)", true, true, true);

		// Municipio
		rangeAddress = new CellRangeAddress(4,5,15,15);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Municipio", true, false, true);

		// Departamento
		rangeAddress = new CellRangeAddress(4,5,16,16);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Depto", true, false, true);

		// Total
		rangeAddress = new CellRangeAddress(4,5,17,17);
		setBordersToMergedCellsEInicializarCelda(sheet, rangeAddress, "Total", true, false, true);
	}

	/**
	 * Pone el border
	 * @param sheet
	 * @param rangeAddress
	 */
	private void setBordersToMergedCellsEInicializarCelda(XSSFSheet sheet, CellRangeAddress rangeAddress, String valorString, boolean merged, boolean wraped, boolean bold) 
	{
		//Creat celda con merge
		if(sheet.getRow(rangeAddress.getFirstRow()) == null || sheet.getRow(rangeAddress.getLastRow()) == null)
		{
			crearRows(rangeAddress.getFirstRow(), rangeAddress.getLastRow(), sheet);
		}
		if(merged)
		{
			sheet.addMergedRegion(rangeAddress);
		}
		Row row = sheet.getRow(rangeAddress.getFirstRow());
		Cell cell = row.createCell(rangeAddress.getFirstColumn());
		cell.setCellValue(valorString);
		CellStyle style = sheet.getWorkbook().createCellStyle();
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setAlignment(HorizontalAlignment.CENTER);

		if(wraped)
		{
			style.setWrapText(true);
		}

		//A�adir borde
		Font newFont = sheet.getWorkbook().createFont();
		if(bold)
		{
			newFont.setBold(true);
		}
		newFont.setFontHeight((short)(9*20));
		style.setFont(newFont);
		cell.setCellStyle(style);
		RegionUtil.setBorderTop(BorderStyle.THIN, rangeAddress, sheet);
		RegionUtil.setBorderLeft(BorderStyle.THIN, rangeAddress, sheet);
		RegionUtil.setBorderRight(BorderStyle.THIN, rangeAddress, sheet);
		RegionUtil.setBorderBottom(BorderStyle.THIN, rangeAddress, sheet);
	}

	/**
	 * Pone el border
	 * @param sheet
	 * @param rangeAddress
	 */
	private void setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(XSSFSheet sheet, CellRangeAddress rangeAddress, String valorString, boolean merged, boolean wraped, boolean bold) 
	{
		//Creat celda con merge
		if(sheet.getRow(rangeAddress.getFirstRow()) == null || sheet.getRow(rangeAddress.getLastRow()) == null)
		{
			crearRows(rangeAddress.getFirstRow(), rangeAddress.getLastRow(), sheet);
		}
		if(merged)
		{
			sheet.addMergedRegion(rangeAddress);
		}
		Row row = sheet.getRow(rangeAddress.getFirstRow());
		Cell cell = row.createCell(rangeAddress.getFirstColumn());
		cell.setCellValue(valorString);
		CellStyle style = sheet.getWorkbook().createCellStyle();
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setAlignment(HorizontalAlignment.LEFT);

		if(wraped)
		{
			style.setWrapText(true);
		}

		//A�adir borde
		Font newFont = sheet.getWorkbook().createFont();
		if(bold)
		{
			newFont.setBold(true);
		}
		newFont.setFontHeight((short)(9*20));
		style.setFont(newFont);
		cell.setCellStyle(style);
		RegionUtil.setBorderTop(BorderStyle.THIN, rangeAddress, sheet);
		RegionUtil.setBorderLeft(BorderStyle.THIN, rangeAddress, sheet);
		RegionUtil.setBorderRight(BorderStyle.THIN, rangeAddress, sheet);
		RegionUtil.setBorderBottom(BorderStyle.THIN, rangeAddress, sheet);
	}

	/**
	 * Crea las rows en el intervalo
	 * @param firstRow
	 * @param lastRow
	 * @param sheet
	 */
	private void crearRows(int firstRow, int lastRow, XSSFSheet sheet)
	{
		while(lastRow >= firstRow)
		{
			sheet.createRow(firstRow);
			firstRow++;
		}
	}

	/**
	 * Pone el border
	 * @param sheet
	 * @param rangeAddress
	 */
	private void setBordersToCellsEInicializarCeldaBigDecimal(XSSFSheet sheet, int rowNum , int colNum, BigDecimal number, boolean porcentaje, boolean bold) 
	{
		CellStyle style = sheet.getWorkbook().createCellStyle();
		Row row = sheet.getRow(rowNum);
		Cell cell = row.createCell(colNum);
		if (porcentaje) 
		{
			if(number.intValue() == -1)
			{
				cell.setCellValue("*");
			}
			else
			{
				cell.setCellValue(number.doubleValue());
				style.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat("#,##0%"));
			}
		}
		else
		{
			style.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat("#,##0"));
			cell.setCellValue(number.doubleValue());
		}

		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setAlignment(HorizontalAlignment.RIGHT);


		//A�adir borde
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderBottom(BorderStyle.THIN);

		Font newFont = sheet.getWorkbook().createFont();
		if(bold)
		{
			newFont.setBold(true);
		}
		newFont.setFontHeight((short)(9*20));
		style.setFont(newFont);
		cell.setCellStyle(style);
	}

	/**
	 * Pone el border
	 * @param sheet
	 * @param rangeAddress
	 */
	private void setBordersToCellsEInicializarCeldasDeInconsistencia(XSSFSheet sheet, int rowNum , int colNum, int number, int denominador) 
	{
		CellStyle styleNum = sheet.getWorkbook().createCellStyle();
		CellStyle stylePorcentaje = sheet.getWorkbook().createCellStyle();
		Row row = sheet.getRow(rowNum);
		Cell cellNum = row.createCell(colNum);
		Cell cellPorcentaje = row.createCell(colNum+1);
		cellNum.setCellValue(number);
		double numeroDouble = number;
		double denominadorDouble = denominador;
		double result = (numeroDouble/denominadorDouble);
		cellPorcentaje.setCellValue(result);
		stylePorcentaje.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat("#,##0%"));
		styleNum.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat("#,##0"));


		styleNum.setVerticalAlignment(VerticalAlignment.CENTER);
		styleNum.setAlignment(HorizontalAlignment.RIGHT);

		stylePorcentaje.setVerticalAlignment(VerticalAlignment.CENTER);
		stylePorcentaje.setAlignment(HorizontalAlignment.RIGHT);


		//A�adir borde
		styleNum.setBorderTop(BorderStyle.THIN);
		styleNum.setBorderLeft(BorderStyle.THIN);
		styleNum.setBorderRight(BorderStyle.THIN);
		styleNum.setBorderBottom(BorderStyle.THIN);
		stylePorcentaje.setBorderTop(BorderStyle.THIN);
		stylePorcentaje.setBorderLeft(BorderStyle.THIN);
		stylePorcentaje.setBorderRight(BorderStyle.THIN);
		stylePorcentaje.setBorderBottom(BorderStyle.THIN);

		Font newFont = sheet.getWorkbook().createFont();	
		newFont.setFontHeight((short)(9*20));
		styleNum.setFont(newFont);
		stylePorcentaje.setFont(newFont);

		cellNum.setCellStyle(styleNum);
		cellPorcentaje.setCellStyle(stylePorcentaje);
	}

	/**
	 * Imprime la info de las preguntas en excel
	 * @param sheet
	 * @param rowNum
	 * @param voData
	 * @param pregunta
	 */
	private void printEnExcelData(XSSFSheet sheet, int rowNum, VOData voData, String pregunta, boolean esElTotal)
	{
		sheet.createRow(rowNum);

		//Cell pregunta
		CellRangeAddress rangeAddress = new CellRangeAddress(rowNum,rowNum,1,1);
		setBordersToMergedCellsEInicializarCeldaHorizontalAligmentIzquierda(sheet, rangeAddress, pregunta, false, true, esElTotal);

		//Cell necesidad
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 2, voData.getRespuestaNecesidadPlaneacionMunicipio(), false, esElTotal);

		//Cell compromiso definitivo municipio
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 3, voData.getCompromisoDefinitivoMunicipio(), false, esElTotal);

		//Cell presupuesto definitivo municipio
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 4, voData.getPresupuestoDefinitivoMunicipio(), false, esElTotal);

		//Cell compromiso definitivo departamento
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 5, voData.getCompromisoDefinitivoDepartamento(), false, esElTotal);

		//Cell presupuesto definitivo departamento
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 6, voData.getPresupuestoDefinitivoDepartamento(), false, esElTotal);

		//Cell % necesidades cubiertas compromiso
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 7, voData.getPorcentajeNecesidadesCubiertasConCompromiso(), true, esElTotal);

		//Cell seguimiento compromiso municipio
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 8, voData.darEjecucionMunicipio(), false, esElTotal);

		//Cell seguimiento presupuesto municipio
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 9, voData.darPresupuestoEjecucionMunicipio(), false, esElTotal);

		//Cell seguimiento compromiso Departamento
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 10, voData.darEjecucionDepartamento(), false, esElTotal);

		//Cell seguimiento presupuesto Departamento
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 11, voData.darPresupuestoEjecucionDepartamento(), false, esElTotal);

		//Cell % necesidades cubiertas ejecuci�n municipio
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 12, voData.getPorcentajeNecesidadesCubiertasConEjecucionMunicipio(), true, esElTotal);

		//Cell % necesidades cubiertas ejecuci�n depto
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 13, voData.getPorcentajeNecesidadesCubiertasConEjecucionDepartamento(), true, esElTotal);

		//Cell % necesidades cubiertas ejecuci�n
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 14, voData.getPorcentajeNecesidadesCubiertasConEjecucion(), true, esElTotal);

		//Cell % ejecucion / compromisos Municipio
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 15, voData.darPorcentajeEjecucionSobreCompromisosMunicipio(), true, esElTotal);

		//Cell % necesidades cubiertas Departamento
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 16, voData.darPorcentajeEjecucionSobreCompromisosDepartamento(), true, esElTotal);	

		//Cell % necesidades cubiertas total
		setBordersToCellsEInicializarCeldaBigDecimal(sheet, rowNum, 17, voData.darPorcentajeEjecucionSobreCompromisosTotal(), true, esElTotal);
	}
}