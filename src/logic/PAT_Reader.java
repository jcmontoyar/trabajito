package logic;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ooxml.util.SAXHelper;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import VO.VOData;
import VO.VODataNull;
import VO.VOMunicipio;

/**
 * 
 * @author Juli�n Montoya
 * Esta clase se encarga de leer el archivo PAT y guardar los datos en estructuras de datos
 * para su futuro procesamiento
 * 
 * La l�gica de leer el archivo excel fue obtenida de http://poi.apache.org/components/spreadsheet/how-to.html#xssf_sax_api
 */
public class PAT_Reader 
{
	//ATRIBUTOS

	/**
	 * Modela el link donde se ub�ca el archivo PAT
	 */
	public  static final String FILE_PAT = "./docs/data/CONSULTASTABLEROPAT_COMPLETAS.xlsx";

	/**
	 * Modela los departamentos
	 */
	public HashMap<String, HashMap<String, LinkedHashMap<String, VOMunicipio>>> departamentos;

	/**
	 * Modela las preguntas
	 */
	public LinkedList<String> preguntasIndicativas;
	
	/**
	 * Modela las capitales
	 */
	public LinkedHashMap<String, String> capitales;

	/**
	 * El handler
	 */
	private SheetHandler handler;

	//M�TODOS

	/**
	 * Carga los datos y retorna las estructuras de datos con todos los datos all� puestos
	 * @param rutaArchivoPAT
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws OpenXML4JException
	 */
	public void cargarDatos(String rutaArchivoPAT) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	{
		System.out.println("\nCargando...");
		OPCPackage pkg = OPCPackage.open(FILE_PAT);
		XSSFReader r = new XSSFReader( pkg );
		SharedStringsTable sst = r.getSharedStringsTable();

		XMLReader parser = fetchSheetParser(sst);

		// To look up the Sheet Name / Sheet Order / rID,
		//  you need to process the core Workbook stream.
		// Normally it's of the form rId# or rSheet#
		java.io.InputStream sheet2 = r.getSheet("rId1");
		InputSource sheetSource = new InputSource(sheet2);
		parser.parse(sheetSource);
		sheet2.close();
		departamentos = handler.getDepartamentos();
		departamentos.remove("Departamento");
		preguntasIndicativas = handler.darPreguntas();
		preguntasIndicativas.remove("PreguntaIndicativa");
		capitales = handler.darCapitales();
		}

	public XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException, ParserConfigurationException 
	{
		XMLReader parser = SAXHelper.newXMLReader();
		handler = new SheetHandler(sst);
		parser.setContentHandler(handler);
		return parser;
	}

	private static class SheetHandler extends DefaultHandler
	{
		/**
		 * Modela los departamentos
		 */
		private HashMap<String, HashMap<String, LinkedHashMap<String, VOMunicipio>>> departamentos = new HashMap<>();

		/**
		 * Modela todas las preguntas indicativas
		 */
		private LinkedList<String> preguntasIndicativas = new LinkedList<>();
		
		/**
		 * Modela todas las preguntas indicativas
		 */
		private LinkedHashMap<String, String> capitales = new LinkedHashMap<>();

		/**
		 * Modela la columna actual
		 */
		private int columnaActual = 0;

		/**
		 * Modela la pregunta indicativa actual
		 */
		private StringBuilder preguntaIndicativaActual;

		/**
		 * Modela el dep actual
		 */
		private StringBuilder departamentoActual;

		/**
		 * Modela el municipio actual
		 */
		private StringBuilder municipioActual;
		
		/**
		 * Modela el municipio actual
		 */
		private StringBuilder divipola;

		/**
		 * Modela el derecho actual
		 */
		private StringBuilder derechoActual;

		/**
		 * Modela la data actual
		 */
		private VOData dataActual;

		/**
		 * Modela la data actual de nulls
		 */
		private VODataNull dataActualNull;

		/**
		 * Contador
		 */
		private int contador = 0;

		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;

		private SheetHandler(SharedStringsTable sst) 
		{
			this.sst = sst;
		}

		public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException 
		{
			// c => cell
			if(name.equals("c")) 
			{
				// Print the cell reference
				//System.out.print(attributes.getValue("r") + " - ");
				if(columnaActual == 28)
				{
					columnaActual = 1;
				}
				else
				{
					columnaActual++;
				}
				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if(cellType != null && cellType.equals("s"))
				{
					nextIsString = true;
				}
				else
				{
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}

		public void endElement(String uri, String localName, String name) throws SAXException 
		{
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if(nextIsString) 
			{
				int idx = Integer.parseInt(lastContents);
				lastContents = sst.getItemAt(idx).getString();

				nextIsString = false;
			}

			// v => contents of a cell
			// Output after we've seen the string contents
			if(name.equals("v")) 
			{
				guardarDato(columnaActual, lastContents);
			}
		}

		/**
		 * Guarda el dato
		 */
		private void guardarDato(int numCol, String dat)
		{
			if(numCol == 1)
			{
			    divipola = new StringBuilder();
				dataActual = new VOData();
				preguntaIndicativaActual = new StringBuilder();
				municipioActual = new StringBuilder();
				departamentoActual = new StringBuilder();
				derechoActual = new StringBuilder();
				dataActualNull = new VODataNull();
				String last3Digits = dat.substring(dat.length() - 3);
				divipola.append(last3Digits);
			}

			//Col municipio
			else if(numCol == 2)
			{
				municipioActual.append(dat);
			}
			//Col depto
			else if(numCol == 4)
			{
				departamentoActual.append(dat);
				if(!capitales.containsKey(departamentoActual.toString()) && divipola.toString().equals("001"))
				{
					capitales.put(dat, municipioActual.toString());
				}
			}
			//derecho 
			else if(numCol == 5)
			{
				if(dat.contains("Vivienda"))
				{
					derechoActual.append("Vivienda");
				}
				else 
				{
					derechoActual.append(dat);
				}
			}
			//Pregunta indicativa
			else if(numCol == 9)
			{
				if(contador < 101)
				{
					preguntasIndicativas.add(dat);
				}
				preguntaIndicativaActual.append(dat);
			}
			//Respuesta necesidad planeacion municipio
			else if(numCol == 12)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullRespuestaNecesidadPlaneacionMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullRespuestaNecesidadPlaneacionMunicipio(true);
				}
				dataActual.setRespuestaNecesidadPlaneacionMunicipio(ret);
			}
			//compromiso definitivo municipio
			else if(numCol == 14)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullcompromisoDefinitivoMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullcompromisoDefinitivoMunicipio(true);
				}
				dataActual.setCompromisoDefinitivoMunicipio(ret);
			}
			//presupuesto definitivo municipio
			else if(numCol == 16)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullpresupuestoDefinitivoMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullpresupuestoDefinitivoMunicipio(true);
				}
				dataActual.setPresupuestoDefinitivoMunicipio(ret);
			}
			//compromiso definitivo departamento
			else if(numCol == 18)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullcompromisoDefinitivoDepartamento(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullcompromisoDefinitivoDepartamento(true);
				}
				dataActual.setCompromisoDefinitivoDepartamento(ret);
			}
			//presupuesto definitivo departamento
			else if (numCol == 20)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullPresupuestoDefinitivoDepartamento(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullPresupuestoDefinitivoDepartamento(true);
				}
				dataActual.setPresupuestoDefinitivoDepartamento(ret);
			}
			//seguimientoCompromisoPrimerSemMunicipio
			else if(numCol == 21)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoCompromisoPrimerSemMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoCompromisoPrimerSemMunicipio(true);
				}
				dataActual.setSeguimientoCompromisoPrimerSemMunicipio(ret);
			}
			//seguimientoCompromisoSegundoSemMunicipio
			else if(numCol == 23)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoCompromisoSegundoSemMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoCompromisoSegundoSemMunicipio(true);
				}
				dataActual.setSeguimientoCompromisoSegundoSemMunicipio(ret);
			}
			//seguimientoPresupuestoPrimerSemMunicipio
			else if(numCol == 22)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoPresupuestoPrimerSemMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoPresupuestoPrimerSemMunicipio(true);
				}
				dataActual.setSeguimientoPresupuestoPrimerSemMunicipio(ret);
			}
			//seguimientoPresupuestoSegundoSemMunicipio
			else if(numCol == 24)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoPresupuestoSegundoSemMunicipio(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoPresupuestoSegundoSemMunicipio(true);
				}
				dataActual.setSeguimientoPresupuestoSegundoSemMunicipio(ret);
			}
			//seguimientoCompromisoPrimerSemDepartamento
			else if(numCol == 25)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoCompromisoPrimerSemDepartamento(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoCompromisoPrimerSemDepartamento(true);
				}
				dataActual.setSeguimientoCompromisoPrimerSemDepartamento(ret);
			}
			//seguimientoCompromisoSegundoSemDepartamento
			else if(numCol == 27)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoCompromisoSegundoSemDepartamento(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoCompromisoSegundoSemDepartamento(true);
				}
				dataActual.setSeguimientoCompromisoSegundoSemDepartamento(ret);
			}
			//seguimientoPresupuestoPrimerSemDepartamento
			else if(numCol == 26)
			{
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoPresupuestoPrimerSemDepartamento(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoPresupuestoPrimerSemDepartamento(true);
				}
				dataActual.setSeguimientoPresupuestoPrimerSemDepartamento(ret);
			}
			//seguimientoPresupuestoSegundoSemDepartamento
			else if(numCol == 28)
			{
				contador++;
				BigDecimal ret;
				try 
				{
					ret = new BigDecimal(dat);
					dataActualNull.setEsNullSeguimientoPresupuestoSegundoSemDepartamento(false);
				}
				catch (NumberFormatException e) 
				{
					ret = new BigDecimal("0");
					dataActualNull.setEsNullSeguimientoPresupuestoSegundoSemDepartamento(true);
				}
				dataActual.setSeguimientoPresupuestoSegundoSemDepartamento(ret);

				//A�adir dato a la estructura
				//Si no esta el depto en la estructura, entonces se a�ade
				if(!departamentos.containsKey(departamentoActual.toString()))
				{
					//Crear hash de municipios temporal
					HashMap<String, LinkedHashMap<String, VOMunicipio>> tempHashMunicipio = new HashMap<>();

					//Crear municipio
					VOMunicipio tempMunicipio = new VOMunicipio();

					//Crear lista llave valor del municipio
					LinkedHashMap<String, VOData> tempListMunicipio = new LinkedHashMap<>();

					//Crear lista llave valor derechoMun
					LinkedHashMap<String, VOMunicipio> tempListDerecho = new LinkedHashMap<>();

					//Asignar dataNull a data actual
					dataActual.setDataNull(dataActualNull);

					//A�adir data a la lista
					tempListMunicipio.put(preguntaIndicativaActual.toString(), dataActual);
					tempListMunicipio.put("Total", dataActual);

					//Asignar lista al municipio
					tempMunicipio.setData(tempListMunicipio);

					//A�adir municipio a la hash list de derechos
					tempListDerecho.put(derechoActual.toString(), tempMunicipio);

					//A�adir municipio al hash de municipios del depto
					tempHashMunicipio.put(municipioActual.toString(), tempListDerecho);

					//A�adir depto al hash principal
					departamentos.put(departamentoActual.toString(), tempHashMunicipio);						
				}
				//El depto ya existe en el hash
				else
				{
					HashMap<String, LinkedHashMap<String, VOMunicipio>> hashDepto = departamentos.get(departamentoActual.toString());
					if(!hashDepto.containsKey(municipioActual.toString()))
					{
						//Crear municipio
						VOMunicipio tempMunicipio = new VOMunicipio();

						//Crear lista llave valor del municipio
						LinkedHashMap<String, VOData> tempListMunicipio = new LinkedHashMap<>();

						//Crear lista llave valor derechoMun
						LinkedHashMap<String, VOMunicipio> tempListDerecho = new LinkedHashMap<>();

						//Asignar dataNull a data actual
						dataActual.setDataNull(dataActualNull);

						//A�adir data a la lista
						tempListMunicipio.put(preguntaIndicativaActual.toString(), dataActual);
						tempListMunicipio.put("Total", dataActual);

						//Asignar lista al municipio
						tempMunicipio.setData(tempListMunicipio);

						//A�adir municipio a la hash list de derechos
						tempListDerecho.put(derechoActual.toString(), tempMunicipio);

						//A�adir municipio al hash de municipios del depto
						departamentos.get(departamentoActual.toString()).put(municipioActual.toString(), tempListDerecho);
					}
					//El municipio ya esta asignado
					else
					{
						LinkedHashMap<String, VOMunicipio> listMunc = departamentos.get(departamentoActual.toString()).get(municipioActual.toString());
						if(!listMunc.containsKey(derechoActual.toString()))
						{
							//Crear municipio
							VOMunicipio tempMunicipio = new VOMunicipio();

							//Crear lista llave valor
							LinkedHashMap<String, VOData> tempListMunicipio = new LinkedHashMap<>();

							//Asignar dataNull a data actual
							dataActual.setDataNull(dataActualNull);

							//A�adir data a la lista
							tempListMunicipio.put(preguntaIndicativaActual.toString(), dataActual);
							tempListMunicipio.put("Total", dataActual);

							//Asignar lista al objeto
							tempMunicipio.setData(tempListMunicipio);

							departamentos.get(departamentoActual.toString()).get(municipioActual.toString()).put(derechoActual.toString(), tempMunicipio);
						}
						else
						{
							//Asignar dataNull a data actual
							dataActual.setDataNull(dataActualNull);

							VOData datica = departamentos.get(departamentoActual.toString()).get(municipioActual.toString()).get(derechoActual.toString()).getData().put(preguntaIndicativaActual.toString(), dataActual);
							
							//Asegurarnos de que no este repetido el dato
							if(datica == null)
							{
								sumarVODataAlTotal();
							}
						}
					}
				}
			}		
		}

		/**
		 * Suma el VOData actual al total
		 */
		private void sumarVODataAlTotal()
		{
			String total = "Total";
			VOData vototal = departamentos.get(departamentoActual.toString()).get(municipioActual.toString()).get(derechoActual.toString()).getData().get(total);
			VOData replace = vototal.sumarVOData(dataActual);
			departamentos.get(departamentoActual.toString()).get(municipioActual.toString()).get(derechoActual.toString()).getData().remove(total);
			departamentos.get(departamentoActual.toString()).get(municipioActual.toString()).get(derechoActual.toString()).getData().put(total, replace);
		}

		public void characters(char[] ch, int start, int length) 
		{
			lastContents += new String(ch, start, length);
		}
		
		/**
		 * Retorna las capitales
		 * @return capitales
		 */
		public LinkedHashMap<String, String> darCapitales()
		{
			return capitales;
		}

		/**
		 * Retorna las preguntas indicativas
		 * @return preguntas
		 */
		public LinkedList<String> darPreguntas()
		{
			return preguntasIndicativas;
		}

		/**
		 * @return the departamentos
		 */
		public HashMap<String, HashMap<String, LinkedHashMap<String, VOMunicipio>>> getDepartamentos()
		{
			return departamentos;
		}
	}

	//SETTERS AND GETTERS

	/**
	 * @return the departamentos
	 */
	public HashMap<String, HashMap<String, LinkedHashMap<String, VOMunicipio>>> getDepartamentos()
	{
		return departamentos;
	}

	//public static void main (String[] args) throws IOException, SAXException, ParserConfigurationException, OpenXML4JException
	//{
	//cargarDatos(FILE_PAT);
	//HashMap<String, LinkedHashMap<String, VOMunicipio>> municipio = departamentos.get("Antioquia");
	//for (String string2 : municipio.keySet()) 
	//{
	//System.out.println("   " + string2);
	//LinkedHashMap<String, VOMunicipio> derecho = municipio.get(string2);
	//for (String string3 : derecho.keySet()) 
	//{
	//System.out.println("     " + string3);
	//VOMunicipio vomun = derecho.get(string3);
	//for (String string4 : vomun.getData().keySet()) 
	//{
	//System.out.println("           " + string4);
	//System.out.println("           data1 =" + vomun.getData().get(string4).getCompromisoDefinitivoMunicipio());
	//}
	//}
	//}
	//}
}
